help:

## src/hmm/C.cpython-37m-x86_64-linux-gnu.so
src/hmm/C.cpython-37m-x86_64-linux-gnu.so:
	cd src/hmm ; python build.py build_ext --inplace

## docs                           : Make all the docmentation
.PHONY: docs
docs: docs/api/build/html/index.html docs/manual/build/html/index.html

## doc_server                     : Start doc server on http://localhost:8000
.PHONY: doc_server
doc_server:
	sphinx-autobuild --ignore docs/manual/build/ docs/manual/source/ docs/manual/build/html

## doc_server_api                     : Start API doc server on http://localhost:8000
.PHONY: doc_server_api
doc_server_api:
	sphinx-autobuild --ignore docs/api/build/ docs/api/source/ docs/api/build/html

## docs/api/build/html/index.html : Documentation for the api
docs/api/build/html/index.html : docs/api/source/conf.py
	cd docs/api; rm -rf build; make html
	@echo To view: firefox --no-remote docs/api/build/html/index.html

## docs/manual/build/html/index.html : User documentation
docs/manual/build/html/index.html : docs/api/source/conf.py
	cd docs/manual; rm -rf build; make html
	@echo To view: firefox --no-remote docs/manual/build/html/index.html

## LICENSE.txt                    : gpl-3.0 License for distributing the hmm software
LICENSE.txt:
	wget https://www.gnu.org/licenses/gpl-3.0.txt -O $@

## test                           : Discover and run all tests in hmm
.PHONY : test, test_all
test :
	pytest

## test_all                       : Test against all supported versions of python
test_all:
	nox

## check-types                    : Checks type hints
.PHONY : check-types
check-types:
	mypy --no-strict-optional src/hmm/
# --no-strict-optional allows None as default value

## coverage                       : make test coverage report in htmlcov/
# To view: $ firefox --no-remote ~/projects/proj_hmm/htmlcov/index.html &
.PHONY : coverage
coverage :
	rm -rf .coverage htmlcov
	coverage run -m pytest src/hmm/tests
	coverage html  --omit=/nix/store*

## yapf                           : Force google format on all python code
.PHONY : yapf
yapf :
	yapf -i --recursive --style "google" src/hmm

## lint                           : Run pylint and mypy
.PHONY : lint
lint :
	pylint --rcfile pylintrc src/hmm/
	mypy --no-strict-optional src/hmm/

## test_standards                 : Build api documentation and run pylint
.PHONY : test_standards
test_standards :
	pytest test_coding_standards.py
# Debug with: "pylint --rcfile=pylintrc src/hmm" and "cd docs/api; make html"

## clean                          : Remove machine generated files
.PHONY : clean
clean :
	rm -f *.npy

## help                           : Print comments on targets from makefile
.PHONY : help
help : Makefile
	@sed -n 's/^## / /p' $<

###---------------
### Local Variables:
### mode: makefile
### End:
