.. HMM manual documentation master file.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

   Append to sys.path in conf.py to find source files.

   Command line: "make html"

Welcome to HMM's documentation
=====================================================

Here I (Andy Fraser) *explain* code that I am writing to implement
hidden Markov model methods in python using numpy and scipy.  I intend
the code to be generally useful and easy to read.  For clarity, some
of the algorithms are implemented more than once, eg: First for the
simplest cases; Second for more general applications; Third in Cython
for speed.  A companion project relies on the code here to build a new
version of my book `Hidden Markov Models and Dynamical Systems
<https://epubs.siam.org/doi/book/10.1137/1.9780898717747?mobileUi=0>`_
which explains the theory.  Both code projects and a revision of the
book are works in progress.

As a substitute for a *tutorial* and *HowTo*, see the companion
project, hmmds, which does the calculations and builds the figures for
the book.  Ultimately that project will enable one build the entire
book by simply typing *make*.

For details of the API, please see the `reference document
<https://hmm-fraserphysics.readthedocs.io/projects/API/en/latest/index.html>`_
generated from doc-strings in the code.  You can fetch the source code
for `HMM <https://gitlab.com/fraserphysics1/hmm>`_ and the companion
project `hmmds <https://gitlab.com/fraserphysics/hmmds>`_ from GitLab.

Contents
============

.. toctree::

   Basic HMM <Basic_HMM>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
