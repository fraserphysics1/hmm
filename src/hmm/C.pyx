# cython: language_level=3, boundscheck=False
"""C.pyx:

Build with:

$ python build.py build_ext --inplace

"""
# Compiler directives must be before the docstring

import warnings
import typing

import numpy
import scipy.sparse
import cython.parallel
from libc.stdlib cimport abort, malloc, free

import hmm.simple
import hmm.base
import hmm.observe_float

import scipy.sparse

from libc.math cimport sqrt, exp, log

warnings.simplefilter('ignore', scipy.sparse.SparseEfficiencyWarning)

cimport cython, numpy

@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
@cython.cdivision(True)
cdef void _forward(
    int t_start,
    int t_stop,
    double[:,:] state_likelihood,
    double[:] gamma_inv,
    double[:,:] alpha,
    double[:,:] p_state2state,
    double [:] last_0
) nogil:

    # This function can be called from inside a parallel loop, eg,
    # cython.parallel.prange in multi_train, without the GIL, python's
    # infamous Global Interpreter Lock.

    cdef double *forecast  # Scratch space
    cdef double *last
    cdef double *temp  
    cdef int t, i, j, n_states
    cdef double gamma

    n_states = alpha.shape[1]
    forecast = <double *> malloc(n_states * sizeof(double))
    last = <double *> malloc(n_states * sizeof(double))
    if last == NULL:
        abort()
    for i in range(n_states):
        last[i] = last_0[i]
    
    # Each step of this iteration depends on results from the
    # previous step.  You can't do it in parallel.

    for t in range(t_start, t_stop):
        # last is the forecast from t-1
        for i in range(n_states):
            last[i] = last[i]*state_likelihood[t,i]
        gamma = 0.0
        for i in range(n_states):
            gamma = gamma + last[i]
        if gamma == 0.0:  # Flag error and reset state distribution
            gamma_inv[t] = -1.0
            for i in range(n_states):
                last[i] = 1.0
                alpha[t,i] = 1.0
        else:
            gamma_inv[t] = 1/gamma
            for i in range(n_states):
                last[i] /= gamma
                alpha[t,i] = last[i]
        for i in range(n_states):
            forecast[i] = 0
            for j in range(n_states):
                forecast[i] += last[j] * p_state2state[j,i]
        temp = forecast
        forecast = last
        last = temp

    free(last)
    free(forecast)

    
@cython.boundscheck(False)
@cython.wraparound(False)
@cython.nonecheck(False)
cdef void _backward(
    int t_start,
    int t_stop,
    double[:,:] state_likelihood,
    double[:] gamma_inv,
    double[:,:] beta,
    double[:,:] p_state2state) nogil:

    # This function can be called from inside a parallel loop,
    # cython.parallel.prange, without the GIL, python's infamous Global
    # Interpreter Lock

    cdef int t, i, j, n_states
    cdef double gamma
    cdef double *last
    cdef double *backcast
    cdef double *temp

    n_states = beta.shape[1]
    backcast = <double *> malloc(n_states * sizeof(double))
    last = <double *> malloc(n_states * sizeof(double))
    if last == NULL:
        abort()
    for i in range(n_states):
        last[i] = 1.0

    # Each step of this iteration depends on results from the
    # previous step.  You can't do it in parallel.
    for t in range(t_stop - 1, t_start - 1, -1):
        for i in range(n_states):
            beta[t,i] = last[i]
            last[i] *= state_likelihood[t,i]*gamma_inv[t]
        for i in range(n_states):
            backcast[i] = 0
            for j in range(n_states):
                backcast[i] += p_state2state[i,j] * last[j]
        temp = backcast
        backcast = last
        last = temp

    free(last)
    free(backcast)

class HMM(hmm.base.HMM):
    """A Cython subclass of HMM that implments methods forward, backward
    and reestimate for speed"""

    @cython.boundscheck(False)
    def forward(
            self, # HMM
            t_start: int = 0,
            t_stop: int = 0,
            t_skip: int = 0,
            last_0=None
    ):
        """Recursively calculate state probabilities, P(s[t]|y[0:t])

        Args:
            t_start: Use self.state_likelihood[t_start] first
            t_stop: Use self.state_likelihood[t_stop-1] last
            t_skip: Number of time steps from when "last" is valid till t_start
            last_0: Optional initial distribution of states

        Returns:
            Log (base e) likelihood of HMM given entire observation sequence

        Works on a single uninterrupted sequence of observations that
        may be only part of a larger set.  As side effect, it sets
        self.alpha and self.gamma_inv for the same range of t as the
        observations.

        """

        if last_0 is None:
            last_0 = self.p_state_initial
        last = numpy.copy(last_0).reshape(-1)

        if t_stop == 0:
            # Reduces to ignoring t_start and t_stop and operating on
            # a single segment
            assert t_start == 0
            t_stop = len(self.state_likelihood)

        for t in range(t_skip):
            self.p_state2state.step_forward(last)

        _forward(
            t_start,
            t_stop,
            self.state_likelihood,
            self.gamma_inv,
            self.alpha,
            self.p_state2state,
            last
            )

        if self.gamma_inv.min() < 0.0:
            raise RuntimeError(f"Impossible data in forward at {numpy.where(self.gamma_inv < 0)}")
        return -(numpy.log(self.gamma_inv[t_start:t_stop])).sum() # End of forward()

    @cython.boundscheck(False)
    def backward(
            self, # HMM
            t_start=0,
            t_stop=0
    ):
        """
        Baum Welch backwards pass through state conditional likelihoods.


        Calculates values of self.beta which "reestimate()" needs.
        """

        if t_stop == 0:
            # Reduces to ignoring t_start and t_stop and operating on
            # a single segment
            assert t_start == 0
            t_stop = len(self.state_likelihood)

        _backward(
            t_start,
            t_stop,
            self.state_likelihood,
            self.gamma_inv,
            self.beta,
            self.p_state2state,
            )

        return

    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.nonecheck(False)
    @cython.cdivision(True)
    def reestimate(self, # HMM
                   ):
        """Phase of Baum Welch training that reestimates model parameters

        Calculate new model parmeters given the quantities already
        calculated, ie, self.state_likelihood, self.alpha, self.beta,
        and self.gamma_inv, and Py, these calcuations are independent
        of the observation model calculations.  This delegates
        reestimating observation model parameter to a call
        self.y_mod.reestimate()

        """

        # wsum is for new p_state_time_average
        cdef double [:] wsum = numpy.zeros(self.n_states)
        # usum is for new p_state2state
        cdef double [:,:] usum = numpy.zeros((self.n_states,self.n_states))

        # Make views of numpy arrays
        cdef double [:] gamma_inv = self.gamma_inv
        cdef double [:,:] alpha = self.alpha
        cdef double [:,:] beta = self.beta
        cdef double [:,:] state_likelihood = self.state_likelihood
        cdef double [:] _wsum = wsum
        cdef double [:,:] _usum = usum

        cdef int t,i,j
        cdef int n_states = self.n_states
        cdef int n_times = self.y_mod.n_times


        for i in cython.parallel.prange(n_states, nogil=True):
            for t in range(n_times-1):
                for j in range(n_states):
                    _usum[i,j] += alpha[t,i]*beta[t+1,j]*state_likelihood[t+1,j]*gamma_inv[t+1]
        # Two loops to avoid race with alpha.  Parallel over states is
        # a little faster than parallel over times.
        for i in cython.parallel.prange(n_states, nogil=True):
            for t in range(n_times-1):
                alpha[t,i] *= beta[t,i]
                _wsum[i] += alpha[t,i]
        #Alpha[n_times-1,:] *= Beta[n_times-1,:] but Beta[n_times-1,:] = 1
        wsum += self.alpha[n_times-1]
        self.p_state_time_average = numpy.copy(wsum)
        self.p_state_initial = numpy.copy(self.alpha[0])
        for x in (self.p_state_time_average, self.p_state_initial):
            x /= x.sum()
        self.p_state2state.inplace_elementwise_multiply(usum)
        self.p_state2state.normalize()
        # Now, self.alpha[t,state] = prob(t,state|model&data)
        self.y_mod.reestimate(self.alpha)
        return # End of reestimate()


    @cython.wraparound(False)
    @cython.boundscheck(False)
    @cython.nonecheck(False)
    @cython.cdivision(True)
    def multi_train(
            self, # HMM
            y_sequences, n_iterations: int, display=True):
        """Copy of parent except loop over segments runs in parallel.

        Args:
            y_sequences: Measured observation sequences in format appropriate
                for self.y_mod
            n_iterations: The number of iterations to execute

        Returns:
            List of utility (log likelihood or log MAP) per observation
            for each iteration

        """

        # utility_list[i] = log(Prob(y_sequences|HMM[iteration=i]))/n_times, ie,
        # the log likelihood per time step, or MAP
        utility_list = []

        t_seg_p = self.y_mod.observe(y_sequences)  # Segment boundaries
        n_seg = len(t_seg_p) - 1
        self.p_state_initial_all = numpy.empty((n_seg, self.n_states))
        for _seg in range(n_seg):
            self.p_state_initial_all[_seg,:] = self.p_state_initial.copy()
            
        self.n_times = self.y_mod.n_times
        self.state_likelihood = numpy.empty((self.n_times, self.n_states))

        assert self.n_times > 1
        assert t_seg_p[0] == 0

        self.alpha = numpy.empty((self.n_times, self.n_states))
        self.beta = numpy.empty((self.n_times, self.n_states))
        self.gamma_inv = numpy.empty((self.n_times,))
        log_likelihood_p = numpy.empty(n_seg)
        forecast_p = numpy.empty(self.n_states)


        # Make c views of numpy arrays
        cdef double [:] gamma_inv = self.gamma_inv
        cdef double [:,:] alpha = self.alpha
        cdef double [:,:] beta = self.beta
        cdef double [:,:] p_state2state = self.p_state2state
        cdef double [:,:] state_likelihood = self.state_likelihood
        cdef double [:] log_likelihood = log_likelihood_p
        cdef double [:] forecast = forecast_p
        # State probabilities at the beginning of each segment
        cdef double [:,:] p_state_initial_all = self.p_state_initial_all
        cdef long [:] t_seg = t_seg_p
        cdef int n_states = self.n_states
        cdef int n_times = self.n_times
        
        cdef int t, i, j, t_start, t_stop, seg

        for iteration in range(n_iterations):
            message = [f"{iteration:2d} "]
            sum_log_like = 0.0
            self.state_likelihood[:] = self.y_mod.calculate()

            # Operate on each observation segment separately and put
            # the results in the corresponding segement of the alpha,
            # beta and gamma_inv arrays.
            for seg in cython.parallel.prange(n_seg, nogil=True):
                t_start = t_seg[seg]
                t_stop = t_seg[seg + 1]

                _forward(
                    t_start,
                    t_stop,
                    state_likelihood,
                    gamma_inv,
                    alpha,
                    p_state2state,
                    p_state_initial_all[seg]
                )

                _backward(
                    t_start,
                    t_stop,
                    state_likelihood,
                    gamma_inv,
                    beta,
                    p_state2state,
                )
        
                log_likelihood[seg] = 0.0
                for t in range(t_start, t_stop):
                    log_likelihood[seg] = log_likelihood[seg] - log(gamma_inv[t])

                # Prevent fitting state transitions between segments
                gamma_inv[t_start] = 0
            # End of prange loop
            if self.gamma_inv.min() < 0.0:
                raise RuntimeError(f"Impossible data in forward at {numpy.where(self.gamma_inv < 0)}")
            for seg in range(n_seg):
                t_start = t_seg[seg]
                for i in range(n_states):
                    p_state_initial_all[seg, i] = alpha[t_start, i] * beta[t_start, i]
                sum_log_like += log_likelihood[seg]
                message.append(f"L{seg} {(log_likelihood[seg]):10.3e} ")

            # Record/report/check this iteration
            if hasattr(self.y_mod, 'log_prior'):
                temp = self.y_mod.log_prior()
                utility = sum_log_like + temp
                message.append(f"L prior {temp:10.3e} ")
            else:
                utility = sum_log_like
            utility_per = utility / self.n_times
            utility_list.append(utility_per)
            message.append(f"U/n {utility_per:11.4e}")
            self.ensure_monotonic(utility_list, display, ''.join(message))

            self.reestimate()

        self.p_state_initial[:] = self.p_state_initial_all.sum(axis=0)
        self.p_state_initial /= self.p_state_initial.sum()
        return utility_list

    
    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.nonecheck(False)
    @cython.cdivision(True)
    def decode(self, y):
        """Viterbi decode a single observation sequence
        """

        if y is not None:  # Calculate likelihood of data given state
            self.y_mod.observe(y)
            self.n_times = self.y_mod.n_times
            self.state_likelihood = self.y_mod.calculate()
        _n_times, _n_states = self.state_likelihood.shape
        assert self.n_states == _n_states
        assert _n_times > 1
        best_state_sequence_p = numpy.empty(_n_times, numpy.int32)

        # Define c variables and allocate working memory
        cdef int t, i, j, k, previous_best_state
        cdef int n_times = _n_times
        cdef int n_states = _n_states
        cdef int [:,:] best_predecessors = numpy.empty(
            (self.n_times, self.n_states), numpy.int32)
        cdef int [:] best_state_sequence = best_state_sequence_p
        cdef double u_k, u_max
        cdef double [:] best_path_utility = self.state_likelihood[0] * self.p_state_initial
        cdef double [:,:] utility = numpy.empty((_n_states, _n_states))
        cdef double [:,:] state_likelihood = self.state_likelihood
        cdef double [:,:] p_state2state = self.p_state2state

        for t in range(1, n_times):

            # utility[i,j] = utility of best path to i * utility of i -> j
            for i in range(n_states):
                for j in range(n_states):
                    utility[i,j] = p_state2state[i,j] * best_path_utility[i] * state_likelihood[t,j]

            u_max = utility[0,0]
            # Find the best predecessor for each state
            for j in range(n_states):
                k = 0
                u_k = utility[k,j]
                for i in range(1,n_states):
                    if utility[i,j] > u_k:
                        k = i
                        u_k = utility[k,j]
                best_predecessors[t,j] = k
                best_path_utility[j] = u_k
                if u_k > u_max:
                    u_max = u_k
            for j in range(n_states):
                best_path_utility[j] = best_path_utility[j] / u_max  # Prevent underflow

        # Find the best end state
        previous_best_state = numpy.argmax(best_path_utility)

        # Backtrack through best_predecessors to find the best
        # sequence.
        for t in range(n_times - 1, -1, -1):
            best_state_sequence[t] = previous_best_state
            previous_best_state = best_predecessors[t, previous_best_state]
        return best_state_sequence_p


# class name must start with 'csc' to get scipy.sparse.csc_matrix.__init__() to work
class cscProb(scipy.sparse.csc_matrix):
    """Replacement for simple.Prob that stores data in sparse matrix
    format.  P[a,b] is the probability of b given a.
    
    For pruning.  Drop x[i,j] if x[i,j] < threshold*max(A[i,:]) and
    x[i,j] < threshold*max(A[:,j]) I don't understand sensitivity of
    training to pruning threshold.

    """
    def __init__(self, # cscProb
                 x, threshold=-1):
        scipy.sparse.csc_matrix.__init__(self, x)
        self.threshold = threshold
        n,m = self.shape
        self.tcol = numpy.empty(n)  # Scratch space step_back
        self.trow = numpy.empty(m)  # Scratch space step_forward
        self.normalize()
    def values(self # cscProb
    ):
        """ Return dense version of matrix
        """
        return numpy.array(self.todense())
    def assign_col(self, # cscProb
                   i,col):
        """Implements self[:,i]=col.  Very slow because finding each csc[j,i]
        is slow.  However, this is not used in a loop over observations.
        """
        n,m = self.shape
        for j in range(n):
            self[j,i] = col[j]
    def likelihoods(self, # cscProb
                    vector):
        r"""Likelihoods for vector of data

        Args:
            vector: A time series of integer observations

        Returns:
            2-d array of state likelihoods

        If self represents probability of observing integers given
        state, ie, self[s, y] = Probability(observation=y|state=s),
        then this function returns the likelihood for each state given
        the observation at a particular time.  Given T = len(vector) and
        self.shape = (m,n), this returns L with L.shape = (T,m) and L[t,a] =
        Prob(vector[t]|a) \forall t \in [0:T] and a in [0:m].

        """
        n,m = self.shape
        T = len(vector)
        L = numpy.zeros((T,n))
        for t in range(T):
            i = vector[t]
            for j in range(self.indptr[i],self.indptr[i+1]):
                J = self.indices[j]
                L[t,J] = self.data[j]
        return L
    def inplace_elementwise_multiply(self, # cscProb
                                     a):
        """Replace self with product of self and argument.
        """
        n,m = self.shape
        for i in range(m):
            for j in range(self.indptr[i],self.indptr[i+1]):
                self.data[j] *= a[self.indices[j],i]

    def utility(self,  # cscProb
             nu, likelihood_s):
        """Efficient calculation of numpy.outer(nu, likelihood_s)*self (* is
        element-wise).  Used in Viterbi decoding.

        Args:
            nu:  Utility of maximum utility path to each state
            likelihood_s: Likelihood of each state given data y[t]
        Returns:
            Maximum utilities for sequences ending in state pairs

        Used in Viterbi decoding with self[a,b] =
        Prob(s[t+1]=b|s[t]=a).  If nu[a] = maximum utility of s[t-1]=a
        given the data y[0:t] and likelihood_s[b] = Probability observation =
        y[t] given s[t]=b, then this method returns a 2-d array, C,
        with C[a,b] = utility of maximum utility path ending with s[t-1]=a,
        s[t]=b given observations y[0:t+1].

        """
        n, m = self.shape
        r = numpy.zeros(self.shape)
        for i in range(m):
            for j in range(self.indptr[i],self.indptr[i+1]):
                J = self.indices[j]
                r[J,i] = self.data[j] * nu[J] * likelihood_s[i]
        return r
    def normalize(self # cscProb
    ):
        """Divide each row, self[j,:], by its sum.  Then prune based on
        threshold.

        """
        n,m = self.shape
        max_row = numpy.zeros(m) # Row of maxima in each column
        max_col = numpy.zeros(n)
        sum_col = numpy.zeros(n) # Column of row sums
        for i in range(m):    # Add up the rows
            for j in range(self.indptr[i],self.indptr[i+1]):
                J = self.indices[j]
                sum_col[J] += self.data[j]
        for i in range(m):    # Normalize the rows
            for j in range(self.indptr[i],self.indptr[i+1]):
                J = self.indices[j]
                self.data[j] /= sum_col[J]
        if self.threshold < 0:
            return
        for i in range(m):    # Find max of the rows and columns
            for j in range(self.indptr[i],self.indptr[i+1]):
                J = self.indices[j]
                x = self.data[j]
                if x > max_row[i]:
                    max_row[i] = x
                if x > max_col[J]:
                    max_col[J] = x
        max_row *= self.threshold
        max_col *= self.threshold
        k = self.indptr[0]
        L = self.indptr[0]
        for i in range(m):
            for j in range(L,self.indptr[i+1]):
                J = self.indices[j]
                x = self.data[j]
                if (x > max_row[i] or x > max_col[J]):
                    self.indices[k] = J
                    self.data[k] = x
                    k += 1
                else:
                    print('Prune')
            L = self.indptr[i+1]
            self.indptr[i+1] = k
    def step_back(self, # cscProb
                  A):
        """ Implements A[:] = self dot A
        """
        cdef double [:] A_ = A
        cdef double [:] data = self.data
        cdef int [:] indices = self.indices
        cdef int [:] indptr = self.indptr
        cdef double [:] t = self.tcol
        cdef int n = self.shape[0]
        cdef int m = self.shape[1]
        cdef int i,j,J
        for j in range(m):
            t[j] = 0
        for i in range(n):
            for j in range(indptr[i],indptr[i+1]):
                J = indices[j]
                t[J] += data[j]*A_[i]
        for i in range(n):
            A_[i] = t[i]
    def step_forward(self, # cscProb
                A):
        """ Implements A[:] = A dot self
        """
        cdef double [:] A_ = A
        cdef double [:] data = self.data
        cdef int [:] indices = self.indices
        cdef int [:] indptr = self.indptr
        cdef double [:] t = self.trow
        cdef int n = self.shape[0]
        cdef int m = self.shape[1]
        cdef int i,j,J
        for i in range(n):
            t[i] = 0
            for j in range(indptr[i],indptr[i+1]):
                J = indices[j]
                t[i] += data[j]*A_[J]
        for i in range(n):
            A_[i] = t[i]
        
def make_prob(x):
    return cscProb(x)

class LinearContext(hmm.observe_float.LinearContext):
    """A Cython subclass that implments methods calculate and
    reestimate for speed

    """

    @cython.boundscheck(False)
    def calculate(self, # LinearContext
            ):
        """
        Calculate likelihoods: self._likelihood[t,i] = P(y(t)|state(t)=i)

        Returns:
            state_likelihood[t,i] \forall t \in [0:n_times] and i \in [0:n_states]

        Assumes a previous call to observe has assigned self._y and allocated
            self._likelihood
        """
        # FixMe: Need term for log of prior probability
        assert self._y.shape == (self.n_times,)

        # Make local variables to replace references to self in loops
        cdef double [:,:] likelihood = self._likelihood
        cdef double [:,:] coefficients = self.coefficients
        cdef double [:,:] context = self.context
        cdef double [:] variance = self.variance
        cdef double [:] norm = self.norm
        cdef double [:] y = self._y
        cdef float small = self.small
        cdef float delta, exponent
        cdef int t, i, state
        cdef int n_times = self.n_times
        cdef int n_states = self.n_states
        cdef int n_coefficients = len(self.coefficients[0])

        for state in cython.parallel.prange(n_states, nogil=True):
            for t in range(n_times):
                delta = -y[t]
                for i in range(n_coefficients):
                    delta = delta + coefficients[state, i] * context[t,i]
                exponent = - delta*delta/(2 * variance[state])
                if exponent < -300:
                    likelihood[t, state] = 0.0
                else:
                    likelihood[t, state] = norm[state] * exp(exponent)
        return self._likelihood

    @cython.boundscheck(False)
    def reestimate(
            self, # LinearContext
            weights # numpy.ndarray
    ):
        """
        Estimate new model parameters.  Requires self._y already assigned

        Args:
            weights: weights[t,s] = Prob(state[t]=s) given data and
                old model

        """
        mask = weights >= self.small  # Small weights confuse the residual
        # calculation in least_squares()
        masked_weights = mask * weights
        _wsum = masked_weights.sum(axis=0)
        _root_weight = numpy.sqrt(
            masked_weights)  # n_times x n_states array of weights
        _w_context = numpy.empty(self.context.shape)
        _w_y = numpy.empty(self._y.shape)
        _fit = numpy.empty(self.context_dimension)

        cdef double [:] wsum = _wsum
        cdef double [:,:] root_weight = _root_weight
        cdef double [:,:] w_context = _w_context
        cdef double [:] w_y = _w_y
        cdef double [:] y = self._y
        cdef double [:,:] context = self.context
        cdef double [:] variance = self.variance
        cdef double [:] norm = self.norm
        cdef double [:,:] coefficients = self.coefficients
        cdef double [:] alpha = self.alpha
        cdef double [:] beta = self.beta
        cdef double [:] fit = _fit
        cdef int n_states = self.n_states
        cdef int n_times = len(self._y)
        cdef int context_dimension = self.context_dimension
        cdef int state, i, t
        cdef float delta, delta_squared, sum_delta_squared
        cdef float pi = numpy.pi

        for state in range(n_states):
            # Call to numpy.linalg.lstsq inside precludes running this
            # loop over states in parallel.
            
            # Prepare weighted arrays for fit of forecast mean
            for t in cython.parallel.prange(n_times, nogil=True):
                w_y[t] = y[t] * root_weight[t,state]
                for i in range(context_dimension):
                    w_context[t,i] = root_weight[t,state] * context[t,i]

            _fit[:], residuals, rank, singular_values = numpy.linalg.lstsq(
                w_context, w_y, rcond=None)
            
            sum_delta_squared = 0.0
            if rank < context_dimension:
                # Keep old coefficients.  Use prior for variance
                wsum[state] = 0.0
                print(f"In LinearContext.reestimate rank={rank}, dimension={context_dimension}, state={state}")
            else:
                for i in range(context_dimension):
                    coefficients[state,i] = fit[i]

                # Calculate the sum of the squared error for fitting the
                # forecast variance.
                for t in cython.parallel.prange(n_times, nogil=True):
                    delta = 0.0
                    for i in range(context_dimension):
                        delta = delta + w_context[t,i] * fit[i]
                    delta = delta - w_y[t] # delta is the forecast error
                    sum_delta_squared += delta*delta

            numerator = 2 * beta[state] + sum_delta_squared
            denominator = 2 * alpha[state] + 2 + wsum[state]
            variance[state] = numerator / denominator
            norm[state] = 1 / sqrt(
                2 * pi * variance[state])


class AutoRegressive(LinearContext, hmm.observe_float.AutoRegressive):
    """Uses cython for reestimate and calculate methods
    """
    pass

class IntegerObservation(hmm.base.IntegerObservation):
    """The simplest observation model: A finite set of integers.
    Implemented with scipy sparse matrices.

    Args:
        py_state: py_state[s,y] is the probability of y given s
        rng: Random number generator.

    """

    _parameter_keys = ['_py_state']

    def __init__(self, #: IntegerObservation,
                 py_state: numpy.ndarray,
                 rng: numpy.random.Generator,
                 warn: typing.Optional[bool] = True):
        self._py_state = make_prob(py_state)  # new type is cscProb
        hmm.base.BaseObservation.__init__(self, rng)
        self._cummulative_y = numpy.cumsum(py_state, axis=1)
        self.n_states = self._py_state.shape[0]
        self.dtype = [numpy.int32]
        self.warn = warn
        return
    def __str__(self # IntegerObservation
                 ):
        return 'py_state =\n%s'%(self._py_state.todense(),)
    # random_out() provided by base

    @cython.boundscheck(False)
    def calculate(
        self,    # IntegerObservation instance
        ):
        """
        Calculate likelihoods: self._likelihood[t,i] = P(y(t)|state(t)=i)

        Returns:
            state_likelihood[t,i] \forall t \in [0:n_times] and i \in [0:n_states]

        Assumes a previous call to observe has assigned self._y and allocated
            self._likelihood
        """
        # Check size and initialize self._likelihood
        y = self._y
        n_y = len(y)
        n_states = self._py_state.shape[0]
        self._likelihood = numpy.zeros((n_y, n_states))

        cdef double [:,:] likelihood = self._likelihood
        cdef int [:] Y = self._y
        cdef double [:] data = self._py_state.data
        cdef int [:] indices = self._py_state.indices
        cdef int [:] indptr = self._py_state.indptr

        cdef int T = n_y
        cdef int t,i,j,J
        for t in range(T):
            i = Y[t]
            for j in range(indptr[i],indptr[i+1]):
                J = indices[j]
                likelihood[t,J] = data[j]
        return self._likelihood # End of p_y_calculate()

    @cython.boundscheck(False)
    def reestimate(self, # IntegerObservation
                 w):
        """
        Estimate new model parameters.  Differs from version in IntegerObservation
        by not updating self.cum_y which simulation requires.

        Parameters
        ----------
        w : array
            w[t,s] = Prob(state[t]=s) given data and old model

        Returns
        -------
        None
        """
        y = self._y
        n_y = len(y)
        if not type(y) == numpy.ndarray:
            y = numpy.array(y, numpy.int32)
        assert(y.dtype == numpy.int32 and y.shape == (n_y,))
        for yi in range(self._py_state.shape[1]):
            self._py_state.assign_col(
                yi, w.take(numpy.where(y==yi)[0], axis=0).sum(axis=0))
        self._py_state.normalize()
        # cumsum doesn't work on sparse
        # self._cummulative_y = numpy.cumsum(self._py_state, axis=1)
        return

# Don't use hmm.base.HMM because it doesn't use
# inplace_elementwise_multiply
class HMM_SPARSE(HMM):
    """HMM code that uses sparse matrices for state to state and state to
    observation probabilities.  API matches hmm.base.HMM

    """
    def __init__(
            self,      # HMM_SPARSE
            p_state_initial,
            p_state_time_average,
            p_state2state: cscProb,
            y_mod: IntegerObservation,
            rng = None):
        hmm.base.HMM.__init__(
            self,
            p_state_initial,
            p_state_time_average,
            p_state2state,
            y_mod,
            rng)
        self.p_state2state = cscProb(self.p_state2state)

    def decode(self, y):
        raise NotImplementedError


    def forward(
            self, # HMM_SPARSE
            _t_start=0,
            _t_stop=0,
            t_skip=0,
            last_0=None
    ):
        """
        Implements recursive calculation of state probabilities given
        observation probabilities.

        Args:
            t_start: Use self.state_likelihood[t_start] first
            t_stop: Use self.state_likelihood[t_stop-1] last
            t_skip: Number of time steps from when "last" is valid till t_start
            last_0: Optional initial distribution of states

        Returns:
            Log (base e) likelihood of HMM given entire observation sequence

        Like HMM.forward except that self.P_SS is sparse.
 

        Bullet points
        -------------

        On entry:

        * self       is an HMM

        * self.state_likelihood    has been calculated

        * self.n_states     is number of states

        Bullet points
        -------------

        On return:

        * 1/self.gamma_inv[t] = Pr{y(t)=y(t)|y_0^{t-1}}
        * self.alpha[t,i] = Pr{s(t)=i|y_0^t}
        * return value is log likelihood of all data

        """
        
        # Make views of numpy arrays
        cdef double [:] gamma_inv = self.gamma_inv
        cdef double [:,:] alpha = self.alpha
        cdef double [:,:] likelihood = self.state_likelihood

        cdef double [:] data = self.p_state2state.data
        cdef int [:] indices = self.p_state2state.indices
        cdef int [:] indptr = self.p_state2state.indptr

        # Make double buffer for calculations
        cdef double *forecast
        cdef double *last
        scratch = numpy.empty((2,self.n_states))
        cdef double [:, :] next_last = scratch

        cdef int t, i, j, J
        cdef int n_states = self.n_states
        cdef int t_start = _t_start
        cdef int t_stop = _t_stop
        cdef double gamma_t
        
        if t_stop == 0:
            # Reduces to ignoring t_start and t_stop and operating on
            # a single segment
            assert t_start == 0
            t_stop = len(self.state_likelihood)

        # Set up initial distribution of states for iteration
        if last_0 is None:
            scratch[0,:] = self.p_state_initial
        else:
            scratch[0,:] = last_0
        for t in range(t_skip):
            # The following replaces last in place with last *
            # p_state2state ** t_skip
            self.p_state2state.step_forward(scratch[0,:])
        scratch[1,:] = scratch[0,:] # Don't know if t_initial is even or odd

        # iterate
        for t in range(t_start, t_stop):
            last = &next_last[t%2,0]
            forecast = &next_last[(t+1)%2,0]
            gamma_t = 0
            for i in range(n_states):
                last[i] = last[i]*likelihood[t,i]
                gamma_t += last[i]
            for i in range(n_states):
                last[i] /= gamma_t
                alpha[t,i] = last[i]
            for i in range(n_states):
                forecast[i] = 0
                for j in range(indptr[i],indptr[i+1]):
                    J = indices[j]
                    forecast[i] += data[j]*last[J]
            gamma_inv[t] = 1/gamma_t
        return -(numpy.log(self.gamma_inv[t_start:t_stop])).sum()
    # End of forward()

    def backward(
            self, # HMM_SPARSE
            _t_start=0,
            _t_stop=0
    ):
        """
        Implements the Baum_Welch backwards pass through state conditional
        likelihoods of the obserations.

        Like HMM.backward except that self.p_state2state is sparse.
        
        Bullet points
        -------------

        On entry:

        * self    is an HMM

        * self.state_likelihood    has been calculated

        Bullet points
        -------------

        On return:

        * for each state i, beta[t,i] = Pr{y_{t+1}^T|s(t)=i}/Pr{y_{t+1}^T}

        """
        
        # Make views of numpy arrays
        cdef double [:] gamma_inv = self.gamma_inv
        cdef double [:,:] beta = self.beta
        cdef double [:,:] likelihood = self.state_likelihood

        cdef double [:] data = self.p_state2state.data
        cdef int [:] indices = self.p_state2state.indices
        cdef int [:] indptr = self.p_state2state.indptr

        # Make double buffer for calculations
        cdef double *forecast
        cdef double *last
        scratch = numpy.ones((2,self.n_states))
        cdef double [:, :] next_last = scratch

        cdef int t, i, j, J
        cdef int n = self.n_states
        cdef int t_start = _t_start
        cdef int t_stop = _t_stop
        
        if t_stop == 0:
            # Reduces to ignoring t_start and t_stop and operating on
            # a single segment
            assert t_start == 0
            t_stop = len(self.state_likelihood)

        # iterate
        for t in range(t_stop-1,t_start-1,-1):
            last = &next_last[t%2,0]
            forecast = &next_last[(t+1)%2,0]
            for i in range(n):
                beta[t,i] = last[i]
                last[i] *= likelihood[t,i]*gamma_inv[t]
                forecast[i] = 0
            for i in range(n):
                for j in range(indptr[i],indptr[i+1]):
                    J = indices[j]
                    forecast[J] += data[j]*last[i]
        return # End of backward()

#--------------------------------
# Local Variables:
# mode: python
# End:
