"""particle.py: Particle filters

"""
from __future__ import annotations  # Enables, eg, (self: System

import typing
import abc  # Abstract Base Class

import numpy
import numpy.linalg
import numpy.random

import hmm.state_space


class System(abc.ABC):
    """Base class for models of stochastic process for particle filtering

    Methods:
        transition(x[t+1],x[t]): Probability x[t] -> x[t+1].  Called f in D&J
        observation(y[t],x[t]): Probability x[t] -> y[t].  Called g in D&J
        importance_0(y[0], rng): Returns sample x[0] and q(x[0]|y[0])
        importance(y[t], x[t-1], rng): Returns sample x[t] and q(x[t]|y[t],x[t-1])
        prior: p(X_0).  Called mu in D&J
        predecessor: x[-1] argument of importance for t=0
        y_dimension: Dimension of each observation
        rng: Random number generator

    D&J refers to "A tutorial on particle filtering and smoothing:
    fifteen years later" by Arnaud Doucet and Adam Johansen".  x is a
    state and y is an observation.

    """

    # This abstract __init__ must be overridden by subclasses.  It is
    # useful for establishing the types of some attributes that are
    # used in self.forward_filter.  That lets mypy check types.
    @abc.abstractmethod
    def __init__(self: System, *args, **kwargs):
        self.y_dimension = 0
        self.x_dimension = 0
        self.rng = numpy.random.default_rng(1)

    # pylint: disable = no-member, no-self-use, too-many-locals
    def forward_filter(self: System,
                       y_array: numpy.ndarray,
                       n_particles: int | numpy.ndarray,
                       prior: typing.Callable | None = None,
                       threshold: float = 1.0):
        """Run filter on observations y_array

        Args:
            y_array:
            n_particles: Single int or array
            prior:
            threshold: Resample if effective_sample_size < threshold * n_particles

        Returns:
            (particles, means, covariances, log_likelihood)

        log_likelihood = log(prob(y[0:n_times]|model))
        Note: particles.shape=(N_particles, N_observations)
        """
        if prior is None:
            prior = self.prior
        n_times, check_dim = y_array.shape
        assert check_dim == self.y_dimension

        if isinstance(n_particles, int):
            n_particles = numpy.ones(n_times, dtype=int) * n_particles
        assert n_particles.dtype == numpy.dtype('int64')
        assert n_particles.shape == (n_times,)

        weights = numpy.empty(n_particles[0])

        means = numpy.empty((n_times, self.x_dimension))
        covariances = numpy.empty((n_times, self.x_dimension, self.x_dimension))

        # Initialize at t=0
        particles = numpy.empty((n_particles[0], n_times, self.x_dimension))

        # Draw particles and calculate weights for EV_{prior}
        for i in range(n_particles[0]):
            # x_i_0 is a draw from q, and q_i_0 is q(x_i_0)
            x_i_0, q_i_0 = self.importance_0(y_array[0])
            weights[i] = prior(x_i_0) / q_i_0
            particles[i, 0, :] = x_i_0
        weights /= weights.sum()
        # Now EV_{prior} f(x_0) \approx \sum_i weights[i] f(particles[i,0,:])

        likelihood_0 = 0
        # likelihood_0 = EV_{prior} p(y_0|x_0)

        # Calculate likelihood_0 and weights for EV_{x_0|y_0}
        for i, particle in enumerate(particles):
            x_i_0 = particle[0, :]
            likelihood_i = self.observation(y_array[0], x_i_0)
            likelihood_0 += weights[i] * likelihood_i
            weights[i] *= likelihood_i
        weights = weights / weights.sum()

        # Finish work for t=0
        log_like = numpy.log(likelihood_0)
        means[0, :], covariances[0, :, :] = moments(particles[:, 0, :], weights)
        particles, weights = resample(particles, weights, self.rng,
                                      n_particles[0])

        # Iterate t_previous=0, ... t_previous=T-2
        for t_previous, y_t in enumerate(y_array[1:]):
            t_now = t_previous + 1
            likelihood_now = 0

            # Draw particles and calculate weights for EV_{x[t_now]|y[:t_now]}
            for i, particle in enumerate(particles):
                predecessor = particle[t_previous]
                x_i_t, q_i_t = self.importance(y_t, predecessor)
                particles[i, t_now, :] = x_i_t
                weights[i] *= self.transition(x_i_t, predecessor) / q_i_t
            weights /= weights.sum()
            # Now EV_{x_{t_now}|y[0:t_now]} f(x_{t_now}) \approx
            # \sum_i weights[i] f(particles[i,0,:])

            # Calculate likelihood_now and weights for EV_{x[t_now]|y[:t_now+1]}
            for i, particle in enumerate(particles):
                x_i_t = particle[t_now, :]
                likelihood_i = self.observation(y_array[t_now], x_i_t)
                likelihood_now += weights[i] * likelihood_i
                weights[i] *= likelihood_i

            # Finish up work for t=t_now
            log_like += numpy.log(likelihood_now)
            weights = weights / weights.sum()
            means[t_now, :], covariances[t_now, :, :] = moments(
                particles[:, t_now, :], weights)
            particles, weights = resample(particles, weights, self.rng,
                                          n_particles[t_now], threshold)
        return particles, means, covariances, log_like

    @abc.abstractmethod
    def transition(self: System, x_next: numpy.ndarray,
                   x_now: numpy.ndarray) -> float:
        """Calculate and return state transition probability
        Args:
            x_next: State at next time step
            x_now: State at present time step

        Return:
            Probability x_now -> x_next

        """
        raise RuntimeError("Use subclass that defines transition()")

    def observation(self: System, y_now: numpy.ndarray,
                    x_now: numpy.ndarray) -> float:
        """Calculate and return probability of observation given state
        Args:
            x_now: State at present time step
            y_now: State at present time step

        Return:
            Probability x_now -> y_now

        """
        raise RuntimeError("Use subclass that defines observation()")

    @abc.abstractmethod
    def importance_0(self: System,
                     y_0: numpy.ndarray) -> typing.Tuple[numpy.ndarray, float]:
        """Generate a random x_0 and calculate q(x_0|y_0)
        Args:
            y_0: First observation
        """
        raise RuntimeError("Use subclass that defines importance_0()")

    @abc.abstractmethod
    def importance(self: System, y_next: numpy.ndarray,
                   x_now: numpy.ndarray) -> typing.Tuple[numpy.ndarray, float]:
        """Generate a random x_next and calculate q(x_next|y_next, x_now)
        Args:
            y_next
            x_now
            rng

        Return:
            (x_next, q(x_next|y_next, x_now)

        """
        raise RuntimeError("Use subclass that defines importance()")

    @abc.abstractmethod
    def prior(self: System, x_0: numpy.ndarray) -> float:
        """Initial probability
        Args:
            x_0:

        Return:
            p(x_0)

        """
        raise RuntimeError("Use subclass that defines prior()")


def moments(samples, weights):
    """Calculate mean and covariance

    Args:
        samples[i,j]: jth component of sample i
        weight[i]: Weight of sample i
    """
    mean = (samples.T * weights).sum(axis=1)
    deviations = samples - mean
    covariance = numpy.einsum('ki,kj,k->ij', deviations, deviations, weights)
    return mean, covariance


def test_moments():
    """Code for debugging
    """
    samples = numpy.ones((10, 2))
    weights = numpy.ones(10)
    mean, covariance = moments(samples, weights)
    assert numpy.array_equal(mean, numpy.ones(2))
    assert numpy.array_equal(covariance, numpy.zeros((2, 2)))
    samples[:5] *= 0
    mean, covariance = moments(samples, weights)
    assert numpy.array_equal(mean, numpy.ones(2) / 2)
    assert numpy.array_equal(covariance, numpy.ones((2, 2)) / 4)
    return 0


def resample(particles: numpy.ndarray,
             weights: numpy.ndarray,
             rng,
             n_new=None,
             threshold=1.0) -> typing.Tuple[numpy.ndarray, numpy.ndarray]:
    """Draw new particles with uniform weights

    Args:
        particles[i, t, :]: Ith particle at time t
        weights[i]: Normalized weight of particle i
        n_new: Number of particles to return
        threshold: Do not resample if effective sample size > threshold*n_new

    Returns:
        New particles, unnormalized weights of new particles

    """
    n_particles, n_samples, x_dim = particles.shape  # pylint: disable = unused-variable
    assert weights.shape == (n_particles,)

    # Normalize old weights and calculate effective sample size
    squared_weights = weights * weights
    effective_sample_size = 1 / squared_weights.sum()

    if n_new is None:
        n_new = n_particles

    if effective_sample_size >= threshold * n_new and n_new == n_particles:
        # Do not resample
        return particles, weights

    cumulative = weights.cumsum()
    cumulative /= cumulative[-1]  # Make sure cumulative[-1] is 1.0
    delta = rng.uniform(0, 1.0 / n_new)
    indices = numpy.searchsorted(
        cumulative, numpy.linspace(delta, 1 - delta, n_new, endpoint=True))
    new_particles = particles[indices]
    new_weights = weights[indices]
    return new_particles, new_weights


def test_resample():
    """For debugging code
    """
    n_particles = 10
    n_times = 2
    dimension = 2
    rng = numpy.random.default_rng(3)
    particles = rng.uniform(size=(n_particles, n_times, dimension))
    weights = numpy.ones(n_particles)
    new_particles, normalized = resample(particles, weights, 0, rng)
    assert numpy.array_equal(particles, new_particles)
    assert numpy.array_equal(normalized, numpy.ones(n_particles) / n_particles)
    normalized[n_particles // 2:] *= 0
    new_particles, new_normalized = resample(particles, normalized, 0, rng)
    for i, particle in enumerate(new_particles):
        assert numpy.array_equal(particles[i // 2], particle)
    assert numpy.array_equal(new_normalized, normalized * 2)
    return 0


class LinearSystem(System):
    """A simple subclass of System for a linear Gaussian system.

    Args:
        state_map:
        state_covariance:
        observation_map:
        observation_covariance:
        initial_mean:
        initial_covariance:
        rng:

    This is a particle implementation of a linear Gaussian system.
    For any actual application of such a system, use
    hmm.state_space.LinearStationary instead of this class.  Use
    this class to compare the performance of a particle filter to
    a Kalman filter.

    """

    # pylint: disable = super-init-not-called
    def __init__(self: LinearSystem, state_map: numpy.ndarray,
                 state_covariance: numpy.ndarray,
                 observation_map: numpy.ndarray,
                 observation_covariance: numpy.ndarray,
                 initial_mean: numpy.ndarray, initial_covariance: numpy.ndarray,
                 rng: numpy.random.Generator):
        self.state_map = state_map
        self.observation_map = observation_map
        self.initial_distribution = hmm.state_space.MultivariateNormal(
            initial_mean, initial_covariance, rng)
        self.rng = rng
        self.y_dimension, self.x_dimension = observation_map.shape
        self.transition_distribution = hmm.state_space.MultivariateNormal(
            numpy.zeros(self.x_dimension), state_covariance, rng)
        self.observation_distribution = hmm.state_space.MultivariateNormal(
            numpy.zeros(self.y_dimension), observation_covariance, rng)

        # Calculate parameters for importance function
        inverse_observation_covariance = numpy.linalg.inv(
            observation_covariance)
        info_y = numpy.linalg.multi_dot([
            observation_map.T, inverse_observation_covariance, observation_map
        ])
        importance_covariance = numpy.linalg.inv(
            numpy.linalg.inv(state_covariance) + info_y)
        self.importance_distribution = hmm.state_space.MultivariateNormal(
            numpy.zeros(self.x_dimension), importance_covariance, rng)

        self.importance_gain = numpy.linalg.multi_dot([
            importance_covariance, observation_map.T,
            inverse_observation_covariance
        ])

    def transition(self: LinearSystem, x_next, x_now):
        """Calculate the probability of x_next given x_now
        """
        return self.transition_distribution(x_next -
                                            numpy.dot(self.state_map, x_now))

    def observation(self: LinearSystem, y_now, x_now):
        return self.observation_distribution(
            y_now - numpy.dot(self.observation_map, x_now))

    def importance_0(self: LinearSystem,
                     y_0: numpy.ndarray) -> typing.Tuple[numpy.ndarray, float]:
        """Generate an x_0 and calculate q(x_0|y_0)
        Args:
            y_0

        Return:
            (x_0, q(x_0|y_0)

        q(x_0|y_0) = p(x_0)

        Notice that y_0 is ignored in q(x_0|y_0)"""
        x_0 = self.initial_distribution.draw()
        q_value = self.initial_distribution(x_0)
        return x_0, q_value

    def importance(self: LinearSystem, y_next: numpy.ndarray,
                   x_now: numpy.ndarray) -> typing.Tuple[numpy.ndarray, float]:
        """Generate a random x_next and calculate q(x_next|y_next, x_now)
        Args:
            y_next
            x_now

        Return:
            (x_next, q(x_next|y_next, x_now)

        q(x_next|y_next, x_now) = p(x_next|y_next, x_now)

        """
        forecast_mean = numpy.dot(self.state_map, x_now)
        forecast_error = y_next - numpy.dot(self.observation_map, forecast_mean)
        update_mean = forecast_mean + numpy.dot(self.importance_gain,
                                                forecast_error)

        noise = self.importance_distribution.draw()
        x_next = update_mean + noise
        q_value = self.importance_distribution(noise)
        return x_next, q_value

    def prior(self: LinearSystem, x_0: numpy.ndarray) -> float:
        return self.initial_distribution(x_0)


#---------------
# Local Variables:
# mode:python
# End:
