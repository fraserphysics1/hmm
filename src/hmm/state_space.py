"""state_space.py: Models with numpy arrays for states and observations.

All Noise assumed to be Gaussian.

Classes:

LinearStationary: Maps for dynamics and observation and associated
    noises given by fixed arrays.

NonStationary: Maps and noises given by functions that take time and
    state as arguments.

SDE: Dynamics and tangent calculated by integrating ODEs

MultivariateNormal:

"""

from __future__ import annotations  # Enables, eg, (self: LinearStationary

import typing

import numpy
import numpy.random
import scipy.integrate


class LinearStationary:
    """State space system model defined by 4 numpy arrays.

    Args:
        a: System dynamics
        b: System noise multiplier
        c: Observation map
        d: Observation noise multiplier
        rng: Random number generator

    Dynamics and observations are linear and noises are Gaussian.
    """

    def __init__(self: LinearStationary, a: numpy.ndarray, b: numpy.ndarray,
                 c: numpy.ndarray, d: numpy.ndarray,
                 rng: numpy.random.Generator):
        # pylint: disable = invalid-name
        self.state_map = a
        self.state_noise_multiplier = b
        self.state_noise_covariance = numpy.dot(b, b.T)
        self.observation_map = c
        self.observation_noise_multiplier = d
        self.observation_noise_covariance = numpy.dot(d, d.T)
        self.rng = rng
        self.y_dim, self.x_dim = self.observation_map.shape
        # pylint thinks these are tuples.  pylint: disable = no-member)
        assert self.state_noise_covariance.shape == (self.x_dim, self.x_dim)
        assert self.observation_noise_covariance.shape == (self.y_dim,
                                                           self.y_dim)

    def __str__(self: LinearStationary) -> str:
        return f"""{type(self)}
state_map:
{self.state_map}
state_noise_multiplier:
{self.state_noise_multiplier}
state_noise_covariance:
{self.state_noise_covariance}
observation_map:
{self.observation_map}
observation_noise_multiplier:
{self.observation_noise_multiplier}
observation_noise_covariance:
{self.observation_noise_covariance}
"""

    def simulate_1_step(
        self: LinearStationary,  # pylint: disable = missing-function-docstring
        x_initial: numpy.ndarray):
        """Map a state forward drawing noise samples for state and observation.

        Args:
            x_initial: A state vector

        Returns:
            (next_x, observation)

        """
        next_x = numpy.dot(self.state_map, x_initial) + numpy.dot(
            self.state_noise_multiplier, self.rng.standard_normal(self.x_dim))
        y = numpy.dot(self.observation_map, next_x) + numpy.dot(
            self.observation_noise_multiplier,
            self.rng.standard_normal(self.y_dim))
        return next_x, y

    def simulate_n_steps(
            self: LinearStationary,
            initial_dist: MultivariateNormal,
            n_samples: int,
            states_0=None) -> typing.Tuple[numpy.ndarray, numpy.ndarray]:
        """Return simulated sequences of states and observations.

        Args:
            initial_dist: Distribution of initial state
            n_samples: Number of states and observations to return

        """
        x = numpy.empty((n_samples, self.x_dim))
        y = numpy.empty((n_samples, self.y_dim))
        if states_0 is None:
            x[0] = initial_dist.draw()
        else:
            x[0] = states_0
        y[0] = numpy.dot(self.observation_map, x[0]) + numpy.dot(
            self.observation_noise_multiplier,
            self.rng.standard_normal(self.y_dim))
        for t in range(1, n_samples):
            x[t], y[t] = self.simulate_1_step(x[t - 1])
        return x, y

    def log_likelihood(self: LinearStationary, prior: MultivariateNormal,
                       y_array: numpy.ndarray) -> float:
        """Report log probability of observations in y_array | model.

        Args:
            prior: Prior for state at t=0
            y_array: Sequence of observations

        Returns:
            log(prob(y_array|model))
        """
        result = 0
        for y in y_array:
            # Kalman filter forward step
            forecast = self.forecast(prior, self.state_map,
                                     self.state_noise_covariance)
            prior = self.update(forecast, y, self.observation_map,
                                self.observation_noise_covariance)
            # Calculate distribution of Y[t]|y[:t]
            mean = numpy.dot(self.observation_map, forecast.mean)
            covariance = quad(
                self.observation_map,
                forecast.covariance) + self.observation_noise_covariance
            y_dist = MultivariateNormal(mean, covariance, self.rng)
            # Evaluate probability density at y[t]
            result += numpy.log(y_dist(y))
        return result

    def forward_filter(self: LinearStationary, initial_dist: MultivariateNormal,
                       y_array: numpy.ndarray) -> tuple:
        """Run Kalman filter on observations y_array.

        Args:
            initial_dist: Prior for state
            y_array: Sequence of observations

        Returns:
            (means, covariances) with means.shape=(T, x_dim) and
            covariances.shape=(T, x_dim, x_dim)) where T=len(y_array)

        """
        means = numpy.empty((len(y_array), self.x_dim))
        covariances = numpy.empty((len(y_array), self.x_dim, self.x_dim))
        forecast = initial_dist
        for t, y in enumerate(y_array):
            update, forecast = self.forward_step(forecast, y)
            means[t] = update.mean
            covariances[t] = update.covariance
        return means, covariances

    def smooth(self: LinearStationary, initial_dist: MultivariateNormal,
               y_array: numpy.ndarray) -> tuple:
        """ Run forward-backward smoothing algorithm

        Args:
            initial_dist: Prior for state
            y_array: Sequence of observations

        Returns:
            (means, covariances) with means.shape=(T, x_dim) and
            covariances.shape=(T, x_dim, x_dim)) where T=len(y_array)
        """

        forward_means, forward_covariances = self.forward_filter(
            initial_dist, y_array)
        backward_means, backward_information = self.backward_information_filter(
            y_array)
        # X[t]|y[:t] ~ \normal(forward_means[t],
        # forward_covariances[t]), and X[t]|y[t+1:] ~
        # \normal(backward_means[t],Sigma[t]) with Sigma[t]^{-1} =
        # backward_information[t]

        smooth_means = numpy.empty(forward_means.shape)
        smooth_covariances = numpy.empty(forward_covariances.shape)

        # For each t, calculate distribution of X[t]|y[:]
        # pylint: disable = consider-using-enumerate
        for t in range(len(smooth_means)):
            forward_information = numpy.linalg.inv(forward_covariances[t])
            smooth_covariances[t] = numpy.linalg.inv(forward_information +
                                                     backward_information[t])

            smooth_means[t] = numpy.dot(
                smooth_covariances[t],
                numpy.dot(forward_information, forward_means[t]) +
                numpy.dot(backward_information[t], backward_means[t]))

        return smooth_means, smooth_covariances

    def forecast(self: LinearStationary,
                 prior: MultivariateNormal,
                 state_map: numpy.ndarray,
                 state_noise_covariance: numpy.ndarray,
                 new_mean=None) -> MultivariateNormal:
        """Forecast the state distribution

        Args:
            prior:
            state_map: Matrix for state dymanics
            noise_covariance: State noise
            new_mean: Optional value for subclasses

        Returns:
            A new distribution
        """

        if new_mean is None:
            mean = numpy.dot(state_map, prior.mean)
        else:
            mean = new_mean
        covariance = quad(state_map, prior.covariance) + state_noise_covariance
        return MultivariateNormal(mean, covariance, self.rng)

    def update(self: LinearStationary,
               forecast: MultivariateNormal,
               y: numpy.ndarray,
               observation_map: numpy.ndarray,
               observation_noise_covariance: numpy.ndarray,
               observation_mean=None) -> MultivariateNormal:
        """Combine observation y[t] with a forecast to get a
        prior for the next step.

        Args:
            forecast: The distibution for x[t] given y[:t]
            y: The observation at time t
            observation_map:
            observation_noise_covariance:
            observation_mean: Optional mean value for subclasses
        """

        if observation_mean is not None:
            y_forecast = observation_mean
        else:
            y_forecast = numpy.dot(observation_map, forecast.mean)
        temp_1 = quad(observation_map, forecast.covariance)
        temp_2 = numpy.linalg.inv(temp_1 + observation_noise_covariance)
        kalman_gain = numpy.dot(forecast.covariance,
                                numpy.dot(observation_map.T, temp_2))
        mean = forecast.mean + numpy.dot(kalman_gain, y - y_forecast)
        temp_3 = numpy.eye(self.x_dim) - numpy.dot(kalman_gain, observation_map)
        # FixMe: Should this be symmetric?  Would temp_4 = (temp_3 +
        # temp_3.T)/2 be a good idea?
        covariance = numpy.dot(temp_3, forecast.covariance)
        return MultivariateNormal(mean, covariance, self.rng)

    def simple_update(self: LinearStationary,
                      forecast: MultivariateNormal,
                      y: numpy.ndarray,
                      observation_map: numpy.ndarray,
                      observation_noise_covariance: numpy.ndarray,
                      observation_mean=None) -> MultivariateNormal:
        """Alternative to update that requires inverting x_dim by x_dim matrices

        Args:
            forecast: The distibution for x[t] given y[:t]
            y: The observation at time t
            observation_map:
            observation_noise_covariance:
        """

        if observation_mean is not None:
            y_forecast = observation_mean
        else:
            y_forecast = numpy.dot(observation_map, forecast.mean)
        inverse_observation_noise_covariance = numpy.linalg.inv(
            observation_noise_covariance)
        covariance = numpy.linalg.inv(
            numpy.linalg.inv(forecast.covariance) +
            quad(observation_map.T, inverse_observation_noise_covariance))

        sigma_inv_diff = numpy.dot(inverse_observation_noise_covariance,
                                   y - y_forecast)
        mean = forecast.mean + numpy.dot(
            covariance, numpy.dot(observation_map.T, sigma_inv_diff))
        return MultivariateNormal(mean, covariance, self.rng)

    def forward_step(
        self: LinearStationary, prior: MultivariateNormal, y: numpy.ndarray
    ) -> typing.Tuple[MultivariateNormal, MultivariateNormal]:
        """Calculate new x_dist based on observation y.

        Args:
            prior: Prior for state
            y: Observation

        Returns:
            (a posteriori distribution for state, and forecast distribution)

        This is a traditional Kalman filter step.
        """
        update = self.update(prior, y, self.observation_map,
                             self.observation_noise_covariance)
        forecast = self.forecast(update, self.state_map,
                                 self.state_noise_covariance)
        return update, forecast

    def backward_information_filter(
            self: LinearStationary,
            y_array: numpy.ndarray,
            mu_beta_last=None) -> typing.Tuple[numpy.ndarray, numpy.ndarray]:
        """Run backwards algorithm keeping track of inverse correlation.

        Args:
            y_array: Sequence of observations
            mu_beta_last: A guess for where to start.

        Returns:
            (mu_beta, phi_beta) arrays of means and informations

        beta[t] is the backcast "distribution" that is a function of y[t+1:]
        b[t] is updated by using y[t]

        Start at t_final = len(y_array)-1 with beta[t_final] =
        (mu_beta_last,Inverse Covariance=0), then iterate.  Since
        initial inverse covariance is 0, mu_beta_last does not
        introduce bias.

        After that initialization, the iteration loop starts at t =
        n_t-2 and goes backward in t with:
        
        update to get b[t+1] = function(beta[t+1], y[t+1])
        backcast to get beta[t] = function(b[t+1])

        Notice that beta[t] depends on y[t+1:] and does not depend on
        y[t].  So using alpha[t] and beta[t] to get the smoothed
        distribution for the state at t, each observation has been
        used exactly once.

        """
        n_t = len(y_array)
        phi_beta = numpy.zeros((n_t, self.x_dim, self.x_dim))
        mu_beta = numpy.zeros((n_t, self.x_dim))
        if mu_beta_last is not None:
            mu_beta[-1] = mu_beta_last

        for t in range(n_t - 2, -1, -1):
            phi_beta[t], mu_beta[t] = self.information_step(
                phi_beta[t + 1], mu_beta[t + 1], y_array[t + 1])
        return mu_beta, phi_beta

    def information_step(
            self: LinearStationary, phi_beta: numpy.ndarray,
            mu_beta: numpy.ndarray,
            y: numpy.ndarray) -> typing.Tuple[numpy.ndarray, numpy.ndarray]:
        """Step backwards tracking the inverse covariance

        Args:
            phi_beta: Inverse covariance of backcast at t+1
            mu_beta: The backcast mean at t+1
            y: Observation at this time

        Returns:
            phi_beta, mu_beta at time t
        """
        phi_b, mu_b = self.information_update(phi_beta, mu_beta, y,
                                              self.observation_map,
                                              self.observation_noise_covariance)

        return self.information_backcast(phi_b, mu_b, self.state_map,
                                         self.state_noise_covariance)

    # pylint: disable = no-self-use
    def information_update(
            self: LinearStationary,
            phi_beta: numpy.ndarray,
            mu_beta: numpy.ndarray,
            y: numpy.ndarray,
            observation_map,
            observation_noise_covariance,
            y_correction=0.0) -> typing.Tuple[numpy.ndarray, numpy.ndarray]:
        """Incorporate the observation at time t.

        Args:
            phi_beta: The backcast inverse covariance at time t
            mu_beta: The backcast mean at t
            y: The observation at time t
            y_correction: observation_mean - numpy.dot(observation_map, mu_beta)


        Returns:
            phi_b, mu_b: updated arrays for time t

        y_correction is for nonlinear subclasses.
        """
        inverse_observation_noise_covariance = numpy.linalg.inv(
            observation_noise_covariance)

        phi_b = phi_beta + quad(observation_map.T,
                                inverse_observation_noise_covariance)

        y_temp = y - y_correction - numpy.dot(observation_map, mu_beta)
        solution = numpy.linalg.lstsq(
            phi_b,
            numpy.dot(observation_map.T,
                      numpy.dot(inverse_observation_noise_covariance, y_temp)),
            rcond=1e-6)[0]

        return phi_b, mu_beta + solution

    def information_backcast(
            self: LinearStationary,
            phi_b,
            mu_b,
            state_map,
            state_noise_covariance,
            nonlinear_mu_beta=None
    ) -> typing.Tuple[numpy.ndarray, numpy.ndarray]:
        """Forecast backwards in time

        Args:
            phi_b: Inverse covariance at time t+1
            mu_b: mean at time t+1
            state_map:
            state_noise_covariance:
            nonlinear_mu_beta: Used by subclasses that calculate it.

        Returns:
            phi_beta, mu_beta: backcast arrays for time t
        """

        inverse_state_map = numpy.linalg.inv(state_map)
        temp_1 = numpy.linalg.inv(
            numpy.eye(self.x_dim) + numpy.dot(phi_b, state_noise_covariance))
        phi_beta = quad(inverse_state_map, numpy.dot(temp_1, phi_b))
        if nonlinear_mu_beta is None:
            mu_beta = numpy.dot(state_map, mu_b)
        else:
            mu_beta = nonlinear_mu_beta
        return phi_beta, mu_beta


class NonStationary(LinearStationary):
    """Maps and noises given by functions that take time and state as arguments.

    Args:
        system: Provides methods that map (t, state) to parameters for calls
            to methods
        dt:  Observation sampling period
        rng: Random number generator

    If the system is non-linear this class does extended Kalman filtering.

    Methods that system must provide:
        simulate(t_initial, t_final, x) -> (x_t_final, y_t_final),
        simulate_n_steps(initial_distribution, n_steps) -> (x_array, y_array)
        forecast(x_initial, t_initial, t_final) -> (x_final, dx_final/dx_initial, state_covariance)
        update(x, t) -> (observation_mean, d observation/d x, observation covariance)
    """

    # pylint: disable = super-init-not-called
    def __init__(self: NonStationary, system, dt, rng):
        self.system = system
        self.dt = dt  # pylint: disable = invalid-name
        self.rng = rng
        self.x_dim = system.x_dim
        self.y_dim = system.y_dim

    def simulate_1_step(self: NonStationary, x_initial: numpy.ndarray) -> tuple:
        """Only useful for stationary system
        """
        return self.system.simulate(0, self.dt, x_initial)

    def simulate_n_steps(
            self: NonStationary,
            initial_dist: MultivariateNormal,
            n_samples: int,
            states_0=None) -> typing.Tuple[numpy.ndarray, numpy.ndarray]:
        """Implementation by self.system.
        """
        return self.system.simulate_n_steps(initial_dist, n_samples, states_0)

    def log_likelihood(self: NonStationary,
                       prior: MultivariateNormal,
                       y_array: numpy.ndarray,
                       per_step=None) -> float:
        """Report log probability of observations in y_array | model

        Args:
            prior: Prior for state at t=0
            y_array: Sequence of periodic (period  self.dt) observations
            per_step: Array to populate with p(y[n]|y[0:n]) forall n

        Returns:
            log(prob(y_array|model))
        """
        result = 0.0
        for n, y in enumerate(y_array):
            f_t, d_f, state_noise_covariance = self.system.forecast(
                prior.mean, 0.0, self.dt)
            g_t, d_g, observation_noise_covariance = self.system.update(
                f_t, self.dt)

            forecast = self.forecast(prior,
                                     d_f,
                                     state_noise_covariance,
                                     new_mean=f_t)
            prior = self.update(forecast,
                                y,
                                d_g,
                                observation_noise_covariance,
                                observation_mean=g_t)
            # Calculate distribution of Y[t]|y[:t]
            covariance = quad(
                d_g, forecast.covariance) + observation_noise_covariance
            y_dist = MultivariateNormal(g_t, covariance, self.rng)
            # Evaluate probability density at y[t]
            argument = y_dist(y)
            if argument <= 0.0:
                raise FloatingPointError(f'prob({y}|model) = {argument}')
            log = numpy.log(argument)
            if per_step is not None:
                per_step[n] = log
            result += log
        return result

    def forward_step(
        self: NonStationary, prior: MultivariateNormal, y: numpy.ndarray
    ) -> typing.Tuple[MultivariateNormal, MultivariateNormal]:
        """Calculate new x_dist based on observation y.

        Args:
            prior: Prior for state
            y: Observation

        Returns:
            (a posteriori distribution for state, and forecast distribution)

        Model is

            new_mean = f(prior.mean)
            x[t+1] ~ (new_mean, F covariance[t] F.T + state_covariance(prior.mean))
            y[t+1] ~ (g(new_mean), G covariance_{x[t+1]} G.T + observation_covariance(new_mean)

        where F is df/dx

        We assume time shift invariance.
        """
        g_t, d_g, observation_noise_covariance = self.system.update(
            prior.mean, 0.0)

        update = self.update(prior,
                             y,
                             d_g,
                             observation_noise_covariance,
                             observation_mean=g_t)

        f_t, d_f, state_noise_covariance = self.system.forecast(
            update.mean, 0.0, self.dt)

        forecast = self.forecast(update,
                                 d_f,
                                 state_noise_covariance,
                                 new_mean=f_t)
        return update, forecast

    def backward_information_filter(
            self: NonStationary,
            y_array: numpy.ndarray,
            mu_beta_last=None) -> typing.Tuple[numpy.ndarray, numpy.ndarray]:
        """Run backwards algorithm keeping track of inverse correlation.

        beta[t] is the backcast "distribution" that is a function of y[t+1:]
        b[t] is updated by using y[t]
        Start at t_final = len(y_array)-1 with beta[t_final] =(0,0), then iterate:
        update to get b[t] = function(beta[t], y[t])
        backcast to get beta[t-1] = function(b[t])
        """
        # Parent Linearstationary method doesn't keep track of times
        n_t = len(y_array)
        times = numpy.linspace(0, n_t * self.dt, n_t, endpoint=False)
        phi_beta = numpy.zeros((n_t, self.x_dim, self.x_dim))
        mu_beta = numpy.zeros((n_t, self.x_dim))
        if mu_beta_last is not None:
            mu_beta[-1] = mu_beta_last
        for t in range(n_t - 2, -1, -1):
            phi_beta[t], mu_beta[t] = self.information_step(
                phi_beta[t + 1], mu_beta[t + 1], y_array[t + 1], times[t + 1],
                times[t])
        return mu_beta, phi_beta

    # pylint: disable = arguments-differ
    # Adds t_initial and t_final to signature of superclass
    def information_step(  # type: ignore[arguments-differ, override]
            self: NonStationary, phi_beta: numpy.ndarray,
            mu_beta: numpy.ndarray, y: numpy.ndarray, t_initial,
            t_final) -> typing.Tuple[numpy.ndarray, numpy.ndarray]:
        """Step backwards tracking the inverse covariance

        Args:
            phi_beta: Inverse covariance of backcast at t+1
            mu_beta: phi_beta * mu_beta where mu_beta is the backcast mean at t+1
            y: Observation at t_initial
            t_initial: Future time that matches phi_beta and mu_beta arguments
            t_final: Present time for return values

        Returns:
            phi_beta, mu_beta at t_final
        """
        # Parent LinearStationary doesn't keep track of time or call self.system*
        assert t_initial > t_final  # Step backwards through time

        # Use observation at t_initial to calculate the updated distribution b
        g_t, d_g, observation_noise_covariance = self.system.update(
            mu_beta, t_initial)
        phi_b, mu_b = self.information_update(phi_beta,
                                              mu_beta,
                                              y,
                                              d_g,
                                              observation_noise_covariance,
                                              y_correction=g_t -
                                              numpy.dot(d_g, mu_beta))

        # Backcast the b distribution to t_final to get the beta
        # distributon and return it.
        f_t, d_f, state_noise_covariance = self.system.forecast(
            mu_b, t_initial, t_final)
        return self.information_backcast(phi_b,
                                         mu_b,
                                         d_f,
                                         state_noise_covariance,
                                         nonlinear_mu_beta=f_t)


class SDE:
    """A system for class NonStationary governed by an ordinary differental equation
    """

    # __init__ needs 11 arguments because SDEs can be complicated

    # pylint: disable = too-many-arguments
    def __init__(  # pylint: disable = invalid-name
        self: SDE,
        f: typing.Callable[[numpy.ndarray], numpy.ndarray],
        tangent: typing.Callable[[numpy.ndarray], numpy.ndarray],
        unit_state_noise: numpy.ndarray,
        observation_function: typing.Callable[[float, numpy.ndarray],
                                              numpy.ndarray],
        observation_noise_multiplier: numpy.ndarray,
        dt: float,  # pylint: disable = invalid-name
        x_dim: int,
        ivp_args: tuple | None = (),
        rng: numpy.random.Generator | None = numpy.random.default_rng(3),
        fudge: float | None = 1.0,
        atol: float | None = 1.0e-6,
        method: str | None = 'RK45',
    ):
        r"""Args:
            f: Maps t,x -> derivative of state wrt time dx/dt
            tangent: Maps t,x -> df/dx
            unit_state_noise: Noise = \sqrt(dt) * unit_state_noise dot Normal(0,I)
            observation_function: Maps t,x -> (y, dy/dx) without noise
            observation_noise_multiplier: Noise = observation_noise_multiplier dot N(0,I)
            dt: Space between samples
            x_dim: Dimension of state vectors
            ivp_args: Additional arguments for solve_ivp
            rng: Random number generator
            fudge: Inflate state noise covariance in filters
            atol: Absolute tolerance for ODE integration
            method: method for ODE integration

        Let \Phi:X,T -> X be a deterministic dynamical system and
        define/calculate:

        d/dt \Phi(x,t)|_{x,0} \equiv f(x)
        d/dx \Phi(x,t)|_{x,0} = Id
        d/dt d/dx \Phi(x,t)|_{x,0} =
        d/dx d/dt \Phi(x,t)|_{x,0} = d/dx f(x)|_x \equiv tangent(x)

        Consider the augmented vector z(x_0,t) =
        [ \Phi(x_0,t) ]
        [ d/dx_0 \Phi(x_0,t) ]

        z(x_0,0) =                            [1]
        [ x_0]
        [ Id ]

        and d/dt z(x_0,t)|_{x_0} =            [2]
        [ f(\Phi(x_0,t)       ]
        [ tangent(\Phi(x_0,t) ]

        Equations [1] and [2] constitute an initial value problem.
        This code calls scipy.integrate.solve_ivp to solve it.

        """
        self.f = f
        self.tangent = tangent
        self.unit_state_noise = unit_state_noise
        self.observation_function = observation_function
        self.observation_noise_multiplier = observation_noise_multiplier
        self.dt = dt
        self.x_dim = x_dim
        self.ivp_args = ivp_args
        self.atol = atol
        self.method = method
        self.y_dim = len(observation_function(0.0, numpy.zeros(x_dim))[0])
        self.rng = rng
        self.fudge = fudge

    def simulate(self: SDE, x_initial: numpy.ndarray, t_initial: float,
                 t_final: float) -> typing.Tuple[numpy.ndarray, numpy.ndarray]:
        """Integrate ODE, add noise, and observe with noise.

        Args:
            x_initial:
            t_initial:
            t_final:

        Returns:
            (x_final, y_final)
        """

        assert x_initial.shape == (self.x_dim,)

        state_noise = numpy.sqrt(t_final - t_initial) * numpy.dot(
            self.unit_state_noise, self.rng.standard_normal(self.x_dim))
        # Extended Kalman filters perform better on time series made
        # by integrating the tangent system.

        x_aug = numpy.empty((1 + self.x_dim) * self.x_dim)
        x_aug[:self.x_dim] = x_initial
        x_aug[self.x_dim:] = numpy.eye(self.x_dim).reshape(-1)
        bunch = scipy.integrate.solve_ivp(self.tangent, (t_initial, t_final),
                                          x_aug,
                                          args=self.ivp_args,
                                          atol=self.atol,
                                          method=self.method)
        x_final = bunch.y[:self.x_dim, -1] + state_noise
        assert x_final.shape == (self.x_dim,)

        observation = self.observation_function(t_final, x_final)[0]
        observation_noise = numpy.dot(
            self.observation_noise_multiplier,
            self.rng.standard_normal(len(observation)))

        return x_final, observation + observation_noise

    def simulate_n_steps(self: SDE, initial_dist, n_samples, states_0=None):
        """Generate arrays of states and observations.
        Args:
            initial_dist: Distribution of initial state
            n_samples: Number of samples to draw
            states_0: State at time 0

        Returns:
            (states, observations)
        """
        states = numpy.empty((n_samples, self.x_dim))
        observations = numpy.empty((n_samples, self.y_dim))
        times = numpy.linspace(0,
                               n_samples * self.dt,
                               n_samples,
                               endpoint=False)
        if states_0 is None:
            states[0] = initial_dist.draw()
        else:
            states[0] = states_0
        observations[0] = self.observation_function(times[0], states[0])[0]
        for i in range(1, n_samples):
            states[i], observations[i] = self.simulate(states[i - 1],
                                                       times[i - 1], times[i])
        return states, observations

    def forecast(
        self: SDE, x_initial: numpy.ndarray, t_initial: float, t_final: float
    ) -> typing.Tuple[numpy.ndarray, numpy.ndarray, numpy.ndarray]:
        """Calculate parameters for forecast step of Kalman filter

        Args:
            x_initial: State at time t_inital
            t_initial:
            t_final:

        Returns:
            (x_final, d x_final/d x_initial, state_covariance)

        """

        assert x_initial.shape == (self.x_dim,)
        # t_initial > t_final if used as backcast
        abs_dt = abs(t_final - t_initial)

        state_noise_covariance = (abs_dt * numpy.dot(
            self.unit_state_noise, self.unit_state_noise.T)) * self.fudge
        assert state_noise_covariance.shape == (self.x_dim, self.x_dim)

        x_aug = numpy.empty((1 + self.x_dim) * self.x_dim)
        x_aug[:self.x_dim] = x_initial
        x_aug[self.x_dim:] = numpy.eye(self.x_dim).reshape(-1)
        bunch = scipy.integrate.solve_ivp(self.tangent, (t_initial, t_final),
                                          x_aug,
                                          args=self.ivp_args,
                                          atol=self.atol,
                                          method=self.method)
        x_final = bunch.y[:self.x_dim, -1]
        derivative = bunch.y[self.x_dim:, -1].reshape((self.x_dim, self.x_dim))
        return x_final, derivative, state_noise_covariance

    def update(self: SDE, state: numpy.ndarray, t: float) -> tuple:
        """Calculate parameters for update step of Kalman filter

        Args:
            state: State vector
            t: Time (might be used for observation function)

        Returns:
            (observation_mean, d observation/d x, observation covariance)
        """
        observation_covariance = numpy.dot(self.observation_noise_multiplier,
                                           self.observation_noise_multiplier.T)
        mean, derivative = self.observation_function(t, state)
        return mean, derivative, observation_covariance

    def relax(
            self: SDE,
            n_samples: int,
            dt=None,  # pylint: disable = invalid-name
            initial_state=None,
            t_initial=0.0) -> typing.Tuple[numpy.ndarray, MultivariateNormal]:
        """Run system for long enough to relax to stationary distribution.

        Args:
            n_samples:
            dt:
            initial_state:
            t_initial:

        Returns:
            (a state, a distribution)

        """
        if dt is None:
            dt = self.dt
        if initial_state is None:
            initial_state = numpy.ones(self.x_dim)
        states = numpy.empty((n_samples, self.x_dim))
        states[0] = initial_state
        for i in range(1, n_samples):
            states[i] = self.simulate(states[i - 1], t_initial + (i - 1) * dt,
                                      t_initial + i * dt)[0]
        return states[-1], make_normal(states, self.rng)


def make_normal(x, rng):
    """Make MultivariateNormal instance by calculating the sample mean
    and covariance of x."""
    n_x, dim_x = x.shape
    assert n_x > dim_x
    mean = numpy.sum(x, axis=0) / n_x
    diffs = x - mean
    covariance = numpy.dot(diffs.T, diffs) / n_x
    assert covariance.shape == ((dim_x, dim_x))
    return MultivariateNormal(mean, covariance, rng)


def quad(x, q_matrix):
    """Calculate quadratic x * q_matrix * x.T
    """
    return numpy.dot(numpy.dot(x, q_matrix), x.T)


def positive_definite(q_in):
    """Test for symmetric and positive definite
    """
    assert numpy.allclose(q_in, q_in.T, atol=1e-4)
    values, _ = numpy.linalg.eigh(q_in)
    assert values.min() > 0


class MultivariateNormal:
    """Multivariate Gaussian distribution

    Args:
        mu: Mean
        sigma: Covariance
    """

    def __init__(self: MultivariateNormal,
                 mu: numpy.ndarray,
                 sigma: numpy.ndarray,
                 rng: numpy.random.Generator |
                 None = numpy.random.default_rng(0)):
        self.mean = mu
        self.covariance = sigma
        self.rng = rng

    def __str__(self: MultivariateNormal) -> str:
        return f"""MultivariateNormal:
mean: {self.mean}
covariance:
{self.covariance}
"""

    def draw(self: MultivariateNormal):
        """Draw a sample from self
        """
        return self.rng.multivariate_normal(self.mean, self.covariance)

    def __call__(self: MultivariateNormal, x: numpy.ndarray) -> float:
        """Calculate the probability density function of self at x
        """
        assert x.shape == self.mean.shape
        dimension = len(self.mean)
        norm = 1 / (
            (2 * numpy.pi)**dimension * numpy.linalg.det(self.covariance))**.5
        delta = x - self.mean
        inv_cov_delta = numpy.linalg.solve(self.covariance, delta)
        exponent = numpy.dot(delta, inv_cov_delta) / -2
        if exponent < -300.0:
            return 0.0
        exponential = numpy.exp(exponent)
        return norm * exponential
