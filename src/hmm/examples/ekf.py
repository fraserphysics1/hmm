"""ekf.py: Models for hmm.state_space.EKF

"""

from __future__ import annotations  # Enables, eg, (self: Linear
import typing

import numpy

import hmm.state_space


class Linear:
    """System for testing hmm.state_space.NonStationary

    Args:
        under: Underlying simple Linear stochastic system
        dt: For checking that all requested times are consistent

    Class Linear exists only for testing.  Class
    hmm.statespace.NonStationary can take an instance of Linear, this
    class, as an argument.  In turn, this class takes an underlying
    LinearStationary instance as an argument.  All computations with
    the NonStationary instance should ultimately be implemented by the
    underlying LinearStationary instance and thus should exactly match
    corresponding computations by a simple LinearStationary instance.

    """

    def __init__(
            self: Linear,
            under: hmm.state_space.LinearStationary,
            dt: float  # pylint: disable = invalid-name
    ):
        self.under = under
        self.dt = dt  # pylint: disable = invalid-name

        self.rng = under.rng
        self.x_dim = under.x_dim
        self.y_dim = under.y_dim
        self.observation_noise_multiplier = under.observation_noise_multiplier
        self.state_noise = under.state_noise_multiplier / numpy.sqrt(dt)
        self.backcast = self.forecast

    def observation_function(
            self: Linear, _,
            state: numpy.ndarray) -> typing.Tuple[numpy.ndarray, numpy.ndarray]:
        """Calculate observation and its derivative
        """
        g_map = self.under.observation_map
        return numpy.dot(g_map, state), g_map

    def observe(self: Linear, _: float, state: numpy.ndarray) -> numpy.ndarray:
        """Add noise to state[0]
        """
        noise = numpy.dot(self.observation_noise_multiplier,
                          self.rng.standard_normal(self.y_dim))
        return self.observation_function(_, state)[0] + noise

    def simulate(self: Linear, t_initial: float, t_final: float,
                 x_initial: numpy.ndarray) -> tuple:
        """Return state and observation

        Args:
            x_initial:
            t_initial:
            t_final:

        Returns:
            (state, observation)

        For class Linear, we use self.under.state_noise_multiplier and
        only use t_initial and t_final to check consistency with
        self.dt.  But for a subclass that integrates an ODE we want
        one simulate method that uses times and supports flexible
        spacing between the times of observations.  Then noise scales
        with sqrt(dt).

        """
        assert self.dt == t_final - t_initial

        multiplier = self.under.state_noise_multiplier
        alternative = numpy.sqrt(self.dt) * self.state_noise
        assert numpy.array_equal(multiplier, alternative)
        state_noise = numpy.dot(alternative,
                                self.under.rng.standard_normal(self.x_dim))
        state = numpy.dot(self.under.state_map, x_initial) + state_noise
        observation = self.observe(t_final, state)
        return state, observation

    def simulate_n_steps(
            self: Linear,
            initial_dist: hmm.state_space.MultivariateNormal,
            n_steps: int,
            states_0=None) -> typing.Tuple[numpy.ndarray, numpy.ndarray]:
        """Integrate ODE and return arrays of states and observations

        Args:
            initial_dist:
            n_steps:

        Returns:
            (states, observations)
        """
        if states_0 is None:
            x_initial = initial_dist.draw()
        else:
            x_initial = states_0
        states = numpy.empty((n_steps, self.x_dim))
        observations = numpy.empty((n_steps, self.y_dim))
        states[0] = x_initial
        observations[0] = self.observe(0, x_initial)
        for i in range(1, n_steps):
            states[i], observations[i] = self.simulate(0.0, self.dt,
                                                       states[i - 1])
        return states, observations

    # pylint: disable = unused-argument
    def forecast(
        self: Linear, x_initial: numpy.ndarray, t_initial: float, t_final: float
    ) -> typing.Tuple[numpy.ndarray, numpy.ndarray, numpy.ndarray]:
        """Calculate parameters for forward step of Kalman filter

        Args:
            x_initial: State at time t_inital
            t_initial:
            t_final:

        Returns:
            (x_final, d x_final/d x_initial, state_covariance)

        """
        d_x = self.under.state_map
        x_final = numpy.dot(d_x, x_initial)
        return x_final, d_x, self.under.state_noise_covariance

    def update(self: Linear, state, _) -> tuple:
        """Calculate parameters for update step of Kalman filter

        Args:
            state: State vector
            _: Time (not used for this stationary system)

        Returns:
            (observation_mean, d observation/d x, observation covariance)
        """
        mean, derivative = self.observation_function(0, state)
        return mean, derivative, self.under.observation_noise_covariance


#---------------
# Local Variables:
# mode:python
# End:
