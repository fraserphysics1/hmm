"""test_state_space.py: To run:
$ python -m pytest src/hmm/tests/test_state_space.py
or to debug:
$ py.test --pdb src/hmm/tests/test_state_space.py

"""
# Copyright (c) 2022 Andrew M. Fraser

import types

import pytest
import numpy
import numpy.testing
import numpy.random

import hmm.state_space

# pylint: disable=missing-function-docstring, missing-class-docstring


class TestQuad:

    def test(self):
        result = hmm.state_space.quad(numpy.ones(2), numpy.ones((2, 2)))
        assert result == 4.0


class TestMultivariateNormal:

    @classmethod
    def setup_class(cls):
        cls.rng = numpy.random.default_rng(5)
        cls.distribution = hmm.state_space.MultivariateNormal(
            numpy.ones(4), numpy.eye(4), cls.rng)

    def test_draw(self):
        result = self.distribution.draw()
        assert result.shape == (4,)
        assert numpy.dot(result, result) == 2.7270648438293317

    def test_call(self):
        result = self.distribution(numpy.ones(4))
        assert result == 0.025330295910584444


class TestLinearStationary:  # pylint: disable=too-few-public-methods
    """Test all methods"""

    # FixMe: I put results from running the code into these tests.
    # That will catch changes to what the code does, but it doesn't
    # catch conceptual errors.  I should think up test cases for which
    # I can write down the expected results without relying on the
    # code I am testing.
    @classmethod
    def setup_class(cls):

        theta = numpy.pi / 8
        cos = numpy.cos(theta)
        sin = numpy.sin(theta)

        state_map = numpy.array([
            [cos, sin, 0],
            [-sin, cos, 0],
            [.5, .5, .5],
        ])
        state_noise = numpy.eye(3)
        observation = numpy.array([[.5, 0, .5], [0, .5, -.5]])
        observation_noise = numpy.eye(2)
        rng = numpy.random.default_rng(5)

        cls.initial = hmm.state_space.MultivariateNormal(
            numpy.zeros(3), numpy.eye(3), rng)
        cls.prior = hmm.state_space.MultivariateNormal(numpy.ones(3),
                                                       numpy.eye(3), rng)

        cls.system = hmm.state_space.LinearStationary(state_map, state_noise,
                                                      observation,
                                                      observation_noise, rng)
        cls.x, cls.y = cls.system.simulate_n_steps(cls.initial, 10)

    def test_simulate(self):
        numpy.testing.assert_allclose(
            self.x,
            numpy.array([[-0.80193143, -1.324359, -0.24836162],
                         [-1.13799188, -1.46930962, -1.97210638],
                         [-1.34087908, -2.15530311, -3.24796914],
                         [-3.79574437, -1.56180442, -4.53530164],
                         [-4.81780058, 0.56302782, -5.00951118],
                         [-3.40575062, 0.72083897, -4.8888721],
                         [-4.16006891, 1.9899831, -3.82477762],
                         [-3.47805928, 2.3391652, -4.35264046],
                         [-1.14785192, 4.20869018, -4.74358396],
                         [0.58317633, 4.37121862, -2.82980264]]))
        assert self.y.shape == (10, 2)

    def test_log_likelihood(self):
        numpy.testing.assert_allclose(
            self.system.log_likelihood(self.initial, self.y),
            -34.824261699143506)

    def test_forward_filter(self):
        means, covariances = self.system.forward_filter(self.initial, self.y)
        assert means.shape == (10, 3)
        assert covariances.shape == (10, 3, 3)
        numpy.testing.assert_allclose(
            means[-1], numpy.array([-1.28463225, 4.59929407, -1.23684214]))
        numpy.testing.assert_allclose(
            covariances[-1, 0, :],
            numpy.array([1.26822915, -0.14763422, -0.05902048]), 1e-5)

    def test_backward_information_filter(self):
        """Run backward algorithm on observations self.y
        """
        means, informations = self.system.backward_information_filter(self.y)
        assert means.shape == (10, 3)
        assert informations.shape == (10, 3, 3)
        numpy.testing.assert_allclose(
            means[0], numpy.array([0.910178, 6.916829, 3.66545]), 1e-6)
        numpy.testing.assert_allclose(
            informations[0, 0, :], numpy.array([0.36776, 0.024169, -0.334722]),
            1e-5, 1e-6)

    def test_smooth(self):
        """Run forward, backward and smooth.
        """
        means, covariances = self.system.smooth(self.initial, self.y)
        assert means.shape == (10, 3)
        assert covariances.shape == (10, 3, 3)
        numpy.testing.assert_allclose(
            means[5], numpy.array([-4.18360582, 7.18793757, 1.64725517]), 1e-5)
        numpy.testing.assert_allclose(
            covariances[5, 0, :],
            numpy.array([0.96464005, -0.10573156, -0.00310776]), 1e-5, 1e-6)

    def test_forecast(self):
        result = self.system.forecast(self.prior, self.system.state_map,
                                      self.system.state_noise_covariance)
        numpy.testing.assert_allclose(result.mean,
                                      numpy.array([1.306563, 0.541196, 1.5]),
                                      1e-5)
        numpy.testing.assert_allclose(result.covariance,
                                      numpy.array([[2., 0., 0.653281],
                                                   [0., 2., 0.270598],
                                                   [0.653281, 0.270598, 1.75]]),
                                      rtol=1e-5,
                                      atol=1e-6)

    def test_information_backcast(self):
        """This tests the forecast backwards in time from the b distribution
           to beta.

        """
        phi_beta, mu_beta = self.system.information_backcast(
            numpy.eye(3), numpy.ones(3), self.system.state_map,
            self.system.state_noise_covariance)
        numpy.testing.assert_allclose(mu_beta,
                                      numpy.array([1.306563, 0.541196, 1.5]),
                                      1e-5)
        numpy.testing.assert_allclose(
            phi_beta,
            numpy.array([[0.5, 0., -0.5], [0., 0.5, -0.5], [-.5, -.5, 3.0]]),
            1e-5, 1e-16)

    def run_update(self, method):
        """Test either system.update or system.simple_update

        Args:
            method: either system.update or system.simple_update
        """
        result = method(self.prior, numpy.ones(2), self.system.observation_map,
                        self.system.observation_noise_covariance)
        numpy.testing.assert_allclose(
            result.mean, numpy.array([1.057143, 1.342857, 0.714286]), 1e-5)
        numpy.testing.assert_allclose(
            result.covariance,
            numpy.array([[0.828571, -0.028571, -0.142857],
                         [-0.028571, 0.828571, 0.142857],
                         [-0.142857, 0.142857, 0.714286]]), 1e-4)

    def test_update(self):
        self.run_update(self.system.update)

    def test_simple_update(self):
        self.run_update(self.system.simple_update)

    def test_forward_step(self):
        forecast = self.system.forward_step(self.prior, numpy.ones(2))[1]
        numpy.testing.assert_allclose(
            forecast.mean, numpy.array([1.49056183, 0.83608717, 1.55714286]),
            1e-5)
        numpy.testing.assert_allclose(
            forecast.covariance,
            numpy.array([[1.80836838, -0.02020305, 0.48396832],
                         [-0.02020305, 1.84877448, 0.30980437],
                         [0.48396832, 0.30980437, 1.57857143]]), 1e-5)

    def test_information_step(self):
        """This test a full step backwards from beta[t+1] -> b[t+1] -> beta[t]
        """
        phi_b, mu_b = self.system.information_step(numpy.eye(3), numpy.ones(3),
                                                   numpy.ones(2))
        numpy.testing.assert_allclose(
            mu_b, numpy.array([1.490562, 0.836087, 1.557143]), 1e-5)
        numpy.testing.assert_allclose(
            phi_b,
            numpy.array([[0.546934, 0.003571, -0.431727],
                         [0.003571, 0.554076, -0.606847],
                         [-0.431727, -0.606847, 3.332631]]), 1e-5, 1e-5)


@pytest.fixture(scope='session')
def sde_setup(dt=0.5,
              n_samples=51,
              y_noise=0.2,
              x_noise=0.01):  # pylint: disable = invalid-name
    """Make an SDE system instance

    Args:
        args: Command line arguments
        dt: Sample interval
        rng:

    Returns:
        (An SDE instance, an initial state, an inital distribution)

    The goal is to get linear_map_simulation.main to exercise all of the
    SDE methods on an ODE that is easy to integrate.

    """

    rng = numpy.random.default_rng(0)

    # State dynamics d/dt x(t) = state_map * x
    state_map = numpy.array([[-.01, .2], [-.2, -.01]])
    # Observation y = observation_map * x
    observation_map = numpy.array([[0, 0.5]])

    observation_noise = numpy.eye(1) * y_noise

    # The next three functions are passed to SDE.__init__
    def observation_function(_, x):
        return numpy.dot(observation_map, x), observation_map

    def dx_dt(_, x, state_map):
        return numpy.dot(state_map, x)

    def tangent(_, x_d, state_map):
        dim_x_d = 6  # 2 for x 4 for d_x
        assert x_d.shape == (dim_x_d,)
        x = x_d[:2]
        derivative = x_d[2:].reshape((2, 2))
        result = numpy.empty(6)
        result[:2] = numpy.dot(state_map, x)
        result[2:] = numpy.dot(state_map, derivative).reshape(-1)
        return result

    state_noise = numpy.eye(2) * x_noise
    dt = 2 * numpy.pi / 10  # 10 samples per cycle
    x_dim = 2
    system = hmm.state_space.SDE(dx_dt,
                                 tangent,
                                 state_noise,
                                 observation_function,
                                 observation_noise,
                                 dt,
                                 x_dim,
                                 ivp_args=(state_map,))
    initial_state = system.relax(500)[0]
    stationary_distribution = system.relax(500, initial_state=initial_state)[1]
    system = hmm.state_space.NonStationary(system, dt, rng)
    _, y_sequence = system.simulate_n_steps(stationary_distribution, n_samples)
    return types.SimpleNamespace(nonstationary=system,
                                 initial=stationary_distribution,
                                 y_=y_sequence)


def test_likelihood(sde_setup):
    sde_setup.nonstationary.log_likelihood(sde_setup.initial, sde_setup.y_)


def test_forward(sde_setup):
    sde_setup.nonstationary.forward_filter(sde_setup.initial, sde_setup.y_)


def test_backward(sde_setup):
    forward_means, _ = sde_setup.nonstationary.forward_filter(
        sde_setup.initial, sde_setup.y_)
    sde_setup.nonstationary.backward_information_filter(sde_setup.y_,
                                                        forward_means[-1])


def test_smooth(sde_setup):
    sde_setup.nonstationary.smooth(sde_setup.initial, sde_setup.y_)


# --------------------------------
# Local Variables:
# mode: python
# End:
