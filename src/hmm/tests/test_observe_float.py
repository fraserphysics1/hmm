"""test_observe_float.py: To run:
$ python -m pytest src/hmm/tests/test_observe_float.py
or
$ python -m pytest src.hmm/tests
to debug:
$ py.test --pdb src/hmm/tests/test_observe_float.py

"""
# Copyright (c) 2021 Andrew M. Fraser
import numpy
import numpy.testing

import hmm.observe_float
import hmm.base
import hmm.C


# pylint: disable=missing-function-docstring, missing-class-docstring
class Parent:  # pylint: disable=too-few-public-methods
    """Don't use this class.  Use subclasses"""

    # If you use method names for this class that begin with test,
    # they will be called with an instance of class Parent even though
    # the name Parent doesn't begin with Test.

    @classmethod
    def setup_class(cls):

        p_initial_state = [0.67, 0.33]
        p_state2state = [[0.93, 0.07], [0.13, 0.87]]
        cls.rng = numpy.random.default_rng(0)
        y_mod_a, y_mod_b = cls.make_y_mods()  # pylint: disable = no-member
        cls.model_a = hmm.base.HMM(p_initial_state, p_initial_state,
                                   p_state2state, y_mod_a, cls.rng)
        cls.model_b = hmm.base.HMM(p_initial_state, p_initial_state,
                                   p_state2state, y_mod_b)

        _, y_train = cls.model_a.simulate(100)
        cls.y_shape = y_train.shape
        cls.y_type = y_train.dtype

        cls.y_train = numpy.array(y_train, numpy.float64)

    def _decode(self, sequence_sum):
        """Called by test_decode in subclasses"""
        return_value = numpy.array(self.model_a.decode((self.y_train,)))
        assert return_value.sum() == sequence_sum

    def _train(self):
        """Called by test_train in subclasses"""
        self.model_b.y_mod.observe((self.y_train,))

        # Exercises calculate and reestimate
        return_value = numpy.array(
            self.model_b.multi_train((self.y_train,), n_iterations=10))

        difference = return_value[1:] - return_value[:-1]
        assert difference.min() > -1.0e-14  # Check monotonic

    def _str(self, reference_string):
        """Called by test_str in subclasses"""
        string = self.model_a.y_mod.__str__()
        n_instance = string.find("instance")
        tail = string[n_instance:]
        assert tail == reference_string


class TestGauss(Parent):

    @classmethod
    def make_y_mods(cls):
        mu_1 = numpy.array([-1.0, 1.0])
        var_1 = numpy.ones(2)
        y_mod_a = hmm.observe_float.Gauss(mu_1.copy(), var_1.copy(), cls.rng)
        y_mod_b = hmm.observe_float.Gauss(mu_1 * 2, var_1 * 4, cls.rng)
        return y_mod_a, y_mod_b

    def test_decode(self):
        self._decode(49)

    def test_train(self):
        self._train()

    def test_str(self):
        self._str("instance:\n    mu\n[-1.  1.]\n    variance\n[1. 1.]\n")


class TestGaussMAP(TestGauss):

    @classmethod
    def make_y_mods(cls):
        mu_1 = numpy.array([-1.0, 1.0])
        var_1 = numpy.ones(2)
        y_mod_a = hmm.observe_float.GaussMAP(mu_1.copy(), var_1.copy(), cls.rng)
        y_mod_b = hmm.observe_float.GaussMAP(mu_1 * 2, var_1 * 4, cls.rng)
        return y_mod_a, y_mod_b


class TestMultivariate(Parent):

    @classmethod
    def make_y_mods(cls):
        mu_1 = numpy.array([[1.0, 1.0], [-1.0, -1.0]])
        mu_2 = numpy.array([[1.0, -1.0], [-1.0, 1.0]])
        var = numpy.array([[[1.0, 0.0], [0.0, 1.0]], [[1.0, 0.0], [0.0, 1.0]]])
        y_mod_a = hmm.observe_float.MultivariateGaussian(
            mu_1.copy(), var.copy(), cls.rng)
        y_mod_b = hmm.observe_float.MultivariateGaussian(
            mu_2.copy(), var.copy(), cls.rng)
        return y_mod_a, y_mod_b

    def test_decode(self):
        self._decode(47)

    def test_train(self):
        self._train()

    def test_str(self):
        self._str(
            "instance\nFor state 0:\n inverse_sigma = \n[[1. 0.]\n [0. 1.]]\n mu = [1. 1.] norm = 0.159155\nFor state 1:\n inverse_sigma = \n[[1. 0.]\n [0. 1.]]\n mu = [-1. -1.] norm = 0.159155\n"  # pylint: disable = line-too-long
        )


class TestVARG(Parent):
    """Vector AutoRegressive with Gaussian residuals.

    VARG is a subclass of VectorContext, and this class tests both
    classes.

    """

    @classmethod
    def make_y_mods(cls):
        a_mean_1 = numpy.array([
            [
                [.9, 0, 0, 1],  #
                [0, .9, 0, 1],
                [0, 0, .9, 1]
            ],
            [
                [-.9, 0, 0, -1],  #
                [0, -.9, 0, -1],
                [0, 0, -.9, -1]
            ],
        ])
        a_mean_2 = numpy.array([
            [
                [.9, 0, 0, 1],  #
                [0, -.9, 0, -1],
                [0, 0, .9, 1]
            ],
            [
                [-.9, 0, 0, -1],  #
                [0, .9, 0, 1],
                [0, 0, -.9, -1]
            ],
        ])

        covariance = numpy.array([
            [
                [1.0, 0.0, 0.0],  #
                [0.0, 1.0, 0.0],
                [0.0, 0.0, 1.0]
            ],
            [
                [1.0, 0.0, 0.0],  #
                [0.0, 1.0, 0.0],
                [0.0, 0.0, 1.0]
            ]
        ]) * 4
        # nu, Psi, and small have default values
        y_mod_1 = hmm.observe_float.VARG(a_mean_1.copy(), covariance.copy(),
                                         cls.rng)
        y_mod_2 = hmm.observe_float.VARG(a_mean_2.copy(), covariance.copy(),
                                         cls.rng)
        return y_mod_1, y_mod_2

    def test_decode(self):
        self._decode(41)

    def test_train(self):
        self._train()

    def test_str(self):
        self._str(
            "instance\nFor state 0:\n a_mean = [[0.9 0.  0.  1. ]\n [0.  0.9 0.  1. ]\n [0.  0.  0.9 1. ]] inverse_sigma = \n[[0.25 0.   0.  ]\n [0.   0.25 0.  ]\n [0.   0.   0.25]]\n Psi = [[0.1 0.  0. ]\n [0.  0.1 0. ]\n [0.  0.  0.1]] nu = 4.0 norm = 0.007937\nFor state 1:\n a_mean = [[-0.9  0.   0.  -1. ]\n [ 0.  -0.9  0.  -1. ]\n [ 0.   0.  -0.9 -1. ]] inverse_sigma = \n[[0.25 0.   0.  ]\n [0.   0.25 0.  ]\n [0.   0.   0.25]]\n Psi = [[0.1 0.  0. ]\n [0.  0.1 0. ]\n [0.  0.  0.1]] nu = 4.0 norm = 0.007937\n"
        )  # pylint: disable = line-too-long


class TestAutoRegressive(Parent):
    """AR order: 1"""

    @classmethod
    def make_y_mods(cls):
        ar_1 = numpy.array([[0.5], [-0.5]])
        offset_1 = numpy.array([0.5, -0.5])
        ar_2 = numpy.array([[0.85], [-0.85]])
        offset_2 = numpy.array([-0.5, 0.5])
        var = numpy.array([1.0, 1.0])

        y_mod_a = hmm.observe_float.AutoRegressive(ar_1.copy(), offset_1.copy(),
                                                   var.copy(), cls.rng)

        y_mod_b = hmm.observe_float.AutoRegressive(ar_2.copy(), offset_2.copy(),
                                                   var.copy(), cls.rng)

        return y_mod_a, y_mod_b

    def test_decode(self):
        self._decode(42)

    def test_train(self):
        self._train()

    def test_str(self):
        self._str(
            "instance\nFor state 0:\n variance = \n1.0\n coefficients = [0.5 0.5] norm = 0.398942\nFor state 1:\n variance = \n1.0\n coefficients = [-0.5 -0.5] norm = 0.398942\n"  # pylint: disable = line-too-long
        )


class TestLinearContext_C(Parent):
    """Tests C.LinearContext using the AutoRegressive subclasses
    """

    ar = numpy.array([[0.5], [-0.5]])
    var = numpy.array([1.0, 1.0])

    @classmethod
    def make_y_mods(cls):
        offset = numpy.array([0.5, -0.5])

        y_mod_a = hmm.observe_float.AutoRegressive(cls.ar.copy(), offset.copy(),
                                                   cls.var.copy(), cls.rng)

        y_mod_b = hmm.C.AutoRegressive(cls.ar.copy(), offset.copy(),
                                       cls.var.copy(), cls.rng)

        return y_mod_a, y_mod_b

    def test_calculate(self):
        self.model_a.y_mod.observe((self.y_train,))
        like_a = self.model_a.y_mod.calculate()
        self.model_b.y_mod.observe((self.y_train,))
        like_b = self.model_b.y_mod.calculate()
        # FixMe: Why such a large rtol?
        numpy.testing.assert_allclose(like_a, like_b, rtol=2e-6)

    def test_reestimate(self):
        self.model_a.y_mod.observe((self.y_train,))
        self.model_a.multi_train((self.y_train,), n_iterations=1)
        self.model_b.y_mod.observe((self.y_train,))
        self.model_b.multi_train((self.y_train,), n_iterations=1)

        for attribute in 'coefficients variance norm'.split():
            for_a = getattr(self.model_a.y_mod, attribute)
            for_b = getattr(self.model_b.y_mod, attribute)
            numpy.testing.assert_allclose(for_a, for_b)

    def test_strip(self):
        self.model_a.strip()
        self.model_b.strip()

    def test_str(self):
        """This just tests observe_float.LinearContext.__str__
        """

        linear_context = hmm.observe_float.LinearContext(
            self.ar, self.var, self.rng)
        string = str(linear_context)
        assert 50 < string.find('coefficients =') < 150


# --------------------------------
# Local Variables:
# mode: python
# End:
