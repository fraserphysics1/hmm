"""conftest.py Makes pytest fixtures that available to test files.

"""

import types

import numpy
import numpy.testing
import numpy.random

import pytest
import scipy.linalg  # type: ignore

import hmm.simple


# pylint: disable = missing-function-docstring
@pytest.fixture(scope="function")
def models():
    """Make objects used by tests.

    """

    n_states = 6
    n_times = 1000
    p_state_initial = numpy.ones(n_states) / float(n_states)
    p_state_time_average = p_state_initial
    p_state2state = scipy.linalg.circulant([0, 0, 0, 0, 0.5, 0.5])
    _py_state = scipy.linalg.circulant([0.4, 0, 0, 0, 0.3, 0.3])

    mask = numpy.ones((n_times, n_states), bool)
    for t in range(n_times):
        mask[t, t % n_states] = False

    rng = numpy.random.default_rng(0)
    simple_observation = hmm.simple.Observation(_py_state.copy(), rng)
    simple_hmm = hmm.simple.HMM(
        p_state_initial.copy(),  # Initial distribution of states
        p_state_time_average.copy(),  # Stationary distribution of states
        p_state2state.copy(),  # State transition probabilities
        simple_observation,
        rng=rng,
    )
    states, observations = simple_hmm.simulate(n_times)
    observations = numpy.array(observations, numpy.int32).reshape(-1)
    result = types.SimpleNamespace(mask=mask,
                                   states=states,
                                   simple_hmm=simple_hmm,
                                   n_times=n_times,
                                   n_states=n_states,
                                   observations=observations)

    result.class2state = {0: [0, 1, 2], 1: [3], 2: [4, 5]}

    return result
