"""test_base.py Tests hmm.base and hmm.C using pytest


$ python -m pytest src/hmm/tests/test_base.py or
$ py.test --pdb src/hmm/tests/test_base.py

"""

from __future__ import annotations

import types

import pytest
import numpy
import numpy.testing
import numpy.random
import scipy

import hmm.base
import hmm.simple
import hmm.C  # type: ignore


# pylint: disable=missing-function-docstring, missing-class-docstring, protected-access, c-extension-no-member
@pytest.fixture(scope="function")
def observation_variables():
    result = types.SimpleNamespace(
        numpy_rng=numpy.random.default_rng(0))  # 0 is seed

    # Make p_ys a 3x2 conditional probability so that p_s dot p_ys
    # gives a distribution of y
    p_ys = hmm.simple.Prob(numpy.array([[0, 1], [1, 1], [1, 3.0]]))
    p_ys.normalize()

    # Make a sequence of binary y values that has somewhat long runs
    n_y = 20
    y = numpy.empty(n_y, dtype=numpy.int32)
    for i in range(n_y):
        y[i] = (i + i % 2 + i % 3 + i % 5) % 2
    result.y = y
    result.y64 = numpy.array(y, dtype=numpy.int64)
    result.y32 = numpy.array(y, dtype=numpy.int32)

    # Set up weights for testing reestimate
    result.weights = numpy.array(n_y * [0, 0, 1.0]).reshape((n_y, 3))
    result.weights[0, :] = [1, 0, 0]
    result.weights[3, :] = [0, 1, 0]

    # Setup segments for testing observe and concatenate
    result.segments = (y[5:], y[3:7], y[:4])

    # Make an integer observation model that handles multiple
    # segments of observations.
    result.y_mod_base = hmm.base.IntegerObservation(p_ys.copy(),
                                                    result.numpy_rng)

    # Make an integer observation model that handles a single
    # sequence of observations.
    result.y_mod_simple = hmm.simple.Observation(p_ys.copy(), result.numpy_rng)

    # Make an integer observation model that handles multiple
    # segments of observations.
    # pylint: disable = invalid-name
    result.y_mod_C = hmm.C.IntegerObservation(p_ys.copy(), result.numpy_rng)

    # Make a tuple (discrete model, observations) from base
    result.y_mod_y_base = (result.y_mod_base, (result.y64,))

    # Make a tuple (discrete model, observations) from simple
    result.y_mod_y_simple = (result.y_mod_simple, result.y64)

    # Make a tuple (discrete model, observations) from C
    result.y_mod_y_C = (result.y_mod_C, (result.y32,))

    ###########Set up for testing ClassObservation and BadObservation###########
    class2state = {
        0: numpy.array([2]),
        1: numpy.array([0, 3]),
        2: numpy.array([4, 1])
    }
    result.class_observation_model = hmm.base.ClassObservation(class2state)
    class_observation_sequence = numpy.array([1, 0, 2, 2])
    # Sequence of observed classes
    possible_states = [class2state[i] for i in class_observation_sequence]
    n_states = 5
    n_t = len(class_observation_sequence)
    result.likelihood_class_observation = numpy.zeros((n_t, n_states))
    for t, states in enumerate(possible_states):
        for state in states:
            result.likelihood_class_observation[t, state] = 1.0
    result.y_class_observation = [class_observation_sequence]

    bad2class = {0: ((0, .2), (1, .2), (2, .6))}
    class2prob = dict(bad2class[0])
    # For state 0 first all three classes are possible
    result.bad_observation_model = hmm.base.BadObservation(
        class2state, bad2class)
    result.likelihood_bad_observation = result.likelihood_class_observation.copy(
    )
    for t, class_ in enumerate(class_observation_sequence):
        result.likelihood_bad_observation[t, 0] = class2prob[class_]
    return result


def test_class_observation(observation_variables):
    assert observation_variables.class_observation_model.random_out(
        4) == 2  # 2 is class for state 4
    observation_variables.class_observation_model.observe(
        observation_variables.y_class_observation)
    assert numpy.array_equal(
        observation_variables.class_observation_model.calculate(),
        observation_variables.likelihood_class_observation)


def test_bad_observation(observation_variables):
    assert observation_variables.bad_observation_model.random_out(4) == 2
    observation_variables.bad_observation_model.observe(
        observation_variables.y_class_observation)
    # return
    assert numpy.array_equal(
        observation_variables.bad_observation_model.calculate(),
        observation_variables.likelihood_bad_observation
    ), f"""{observation_variables.likelihood_bad_observation=}
{observation_variables.bad_observation_model.calculate()=}"""


def calc(observation_variables, y_mod, y):
    y_mod.observe(y)
    p_y = y_mod.calculate()[2:4]
    numpy.testing.assert_equal(p_y, [[0, 0.5, 0.25], [1, 0.5, 0.75]])


def test_calc(observation_variables):
    for y_mod, y in (observation_variables.y_mod_y_C,
                     observation_variables.y_mod_y_base,
                     observation_variables.y_mod_y_simple):
        calc(observation_variables, y_mod, y)


def test_join(observation_variables):
    for y_mod in (observation_variables.y_mod_base,
                  observation_variables.y_mod_C):
        y_mod.observe(observation_variables.segments)
        numpy.testing.assert_equal(y_mod.t_seg, [0, 15, 19, 23])


def reestimate(observation_variables, y_mod, y):
    y_mod.observe(y)
    y_mod.calculate()
    y_mod.reestimate(observation_variables.weights)
    numpy.testing.assert_equal([[1, 0], [0, 1], [5 / 9, 4 / 9]],
                               y_mod._py_state.values())


def test_reestimate(observation_variables):
    for y_mod, y in (observation_variables.y_mod_y_base,
                     observation_variables.y_mod_y_simple,
                     observation_variables.y_mod_y_C):
        reestimate(observation_variables, y_mod, y)


def test_str(observation_variables):
    string = str(observation_variables.y_mod_base)
    assert string.find(' 0.75') == 97


class BaseModel:
    """Provides methods for running and testing generic hmm methods
    """
    # Values here can be accessed by instances of subclasses as, eg,
    # self.n_states.

    n_states = 6
    n_times = 900  # To get training to get close to true parameters
    p_state_initial = numpy.ones(n_states) / float(n_states)
    p_state_time_average = p_state_initial
    p_state2state = scipy.linalg.circulant([0, 0, 0, 0, 0.5, 0.5])
    py_state = scipy.linalg.circulant([0.4, 0, 0, 0, 0.3, 0.3])
    mask = numpy.ones((n_times, n_states), bool)
    for t in range(n_times):
        mask[t, t % n_states] = False

    def __init__(self, hmm_class, observation_class, observation_model=None):
        self.rng = numpy.random.default_rng(0)
        if observation_model is None:
            observation_model = observation_class(self.py_state, self.rng)
        self.hmm = hmm_class(self.p_state_initial,
                             self.p_state_time_average,
                             self.p_state2state,
                             observation_model,
                             rng=self.rng)
        self.state_sequence, observations = self.hmm.simulate(self.n_times)
        if isinstance(observations, hmm.base.JointSegment):
            self.observations = observations
        else:  # For hmm.C.IntegerObservation
            self.observations = numpy.array(observations, dtype=numpy.int32)

    def test_state_simulate(self):
        """Test method defined in hmm.simple
        """
        result1 = self.hmm.state_simulate(self.n_times)
        result2 = self.hmm.state_simulate(self.n_times, self.mask)
        for result in (result1, result2):
            assert len(result) == self.n_times
            array = numpy.array(result)
            assert array.min() == 0
            assert array.max() == self.n_states - 1

    def test_str(self):
        assert 15 < str(self.hmm).find('with 6 states') < 30

    def test_decode(self):
        """
        Check that self.mod gets 70% of the states right
        """
        states = self.hmm.decode([self.observations])
        wrong = numpy.where(states != self.state_sequence)[0]
        assert len(wrong) < len(self.state_sequence) * .3
        return  # FixMe:
        # Check that other models get the same state sequence as self.hmm
        for mod in self.mods[1:]:
            wrong = numpy.where(states != mod.decode(self.observations))[0]
            assert len(wrong) == 0

    def test_train(self):
        """ Test training
        """
        # Do only 3 iterations because testing is slow
        log_like = self.hmm.multi_train([self.observations] * 3,
                                        n_iterations=3,
                                        display=True)
        # Check that log likelihood increases montonically
        for i in range(1, len(log_like)):
            assert log_like[i - 1] < log_like[i]
        # Check that trained model is close to true model
        numpy.testing.assert_allclose(self.hmm.p_state2state.values(),
                                      self.p_state2state,
                                      atol=0.2)
        if hasattr(self.hmm.y_mod, '_py_state'):
            values = self.hmm.y_mod._py_state.values()
        elif 'integer' in self.hmm.y_mod.keys():
            values = self.hmm.y_mod['integer']._py_state.values()
        numpy.testing.assert_allclose(values, self.py_state, atol=0.15)

    def test_t_skip(self):
        self.hmm.y_mod.observe([self.observations])
        # Must assign likelihood because forward uses it
        self.hmm.state_likelihood = self.hmm.y_mod.calculate()
        self.hmm.alpha = numpy.empty((self.n_times, self.n_states))
        self.hmm.gamma_inv = numpy.empty((self.n_times,))
        assert self.hmm.forward(t_skip=5) > -2000.0

    def test_all(self):
        self.test_state_simulate()
        self.test_str()
        self.test_decode()
        self.test_train()
        self.test_t_skip()


def test_base():
    base_model = BaseModel(hmm.base.HMM, hmm.base.IntegerObservation)
    base_model.test_all()
    base_model.hmm.initialize_y_model([base_model.observations])


def test_class_observations():
    """Test base.ClassObservation and base.JointObservation"""

    # Create integer_model just to get its y_mod
    integer_model = BaseModel(hmm.base.HMM,
                              hmm.base.IntegerObservation).hmm.y_mod
    class2state = {0: [0, 1, 2], 1: [3], 2: [4, 5]}
    class_observation_model = hmm.base.ClassObservation(class2state)
    model_dict = {"integer": integer_model, "class": class_observation_model}
    joint_instance = hmm.base.JointObservation(model_dict)
    joint_model = BaseModel(hmm.base.HMM, hmm.base.JointObservation,
                            joint_instance)
    joint_model.test_all()


def compare(models):
    reference = models[0]
    others = models[1:]

    decoded_sequence = reference.hmm.decode([reference.observations])
    state_sequence = reference.state_sequence
    y_sequence = numpy.array(reference.observations)
    reference.test_train()

    for other in others:

        # Check results of hmm.simulate
        assert numpy.array_equal(state_sequence, other.state_sequence)
        assert numpy.array_equal(y_sequence, numpy.array(other.observations))

        other.test_str()

        other_decoded_sequence = other.hmm.decode([other.observations])
        assert numpy.array_equal(decoded_sequence, other_decoded_sequence)

        other.test_train()
        for attribute in 'p_state2state p_state_initial p_state_time_average'.split(
        ):
            assert numpy.allclose(getattr(reference.hmm, attribute),
                                  getattr(other.hmm, attribute))
    return


def test_others():
    other_list = []
    for hmm_class in (hmm.base.HMM, hmm.C.HMM):  # FixMe: neither
        # decode nor multi_train work for hmm.C.HMM_SPARSE
        for observation_class in (hmm.base.IntegerObservation,
                                  hmm.C.IntegerObservation):
            print(f'''{hmm_class=}  {observation_class=}''')
            other_list.append(BaseModel(hmm_class, observation_class))
    compare(other_list)


# --------------------------------
# Local Variables:
# mode: python
# End:
