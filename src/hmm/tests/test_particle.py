"""test_particle.py: To run:
$ python -m pytest src/hmm/tests/test_particle.py
or to debug:
$ py.test --pdb src/hmm/tests/test_particle.py

"""
# Copyright (c) 2022 Andrew M. Fraser

import numpy
import numpy.testing
import numpy.random

import hmm.particle
import hmm.state_space

# pylint: disable=missing-function-docstring, missing-class-docstring, duplicate-code


class TestLinearSystem:  # pylint: disable=too-few-public-methods
    """Test all methods"""

    @classmethod
    def setup_class(cls):

        theta = numpy.pi / 8
        cos = numpy.cos(theta)
        sin = numpy.sin(theta)

        state_map = numpy.array([
            [cos, sin, 0],
            [-sin, cos, 0],
            [.5, .5, .5],
        ])
        state_noise = numpy.eye(3)
        observation_map = numpy.array([[.5, 0, .5], [0, .5, -.5]])
        observation_noise = numpy.eye(2)
        rng = numpy.random.default_rng(5)

        cls.n_times = 10
        cls.initial = hmm.state_space.MultivariateNormal(
            numpy.zeros(3), numpy.eye(3), rng)
        cls.prior = hmm.state_space.MultivariateNormal(numpy.ones(3),
                                                       numpy.eye(3), rng)

        cls.state_space_system = hmm.state_space.LinearStationary(
            state_map, state_noise, observation_map, observation_noise, rng)
        cls.x, cls.y = cls.state_space_system.simulate_n_steps(
            cls.initial, cls.n_times)

        state_covariance = numpy.dot(state_noise, state_noise.T)
        observation_covariance = numpy.dot(observation_noise,
                                           observation_noise.T)
        cls.particle_system = hmm.particle.LinearSystem(
            state_map, state_covariance, observation_map,
            observation_covariance, cls.prior.mean, cls.prior.covariance, rng)
        cls.n_particles = 30

    def test_transition(self):
        """Check the transition probability
        """
        x_now = numpy.ones(self.particle_system.x_dimension)
        x_next = numpy.dot(self.particle_system.state_map, x_now)
        assert 0.063 < self.particle_system.transition(x_next, x_now) < 0.0635

    def test_simulate(self):
        """This just confirms we know what the simulated data from
        hmm.state_space is."""
        assert self.x.shape == (10, 3)
        assert 7 > numpy.abs(self.x).max() > 3

        assert self.y.shape == (10, 2)
        assert 7 > numpy.abs(self.y).max() > 3

    def test_forward_filter(self):
        particles, means, covariances, log_likelihood = self.particle_system.forward_filter(
            self.y, self.n_particles, self.initial)

        assert particles.shape == (self.n_particles, self.n_times,
                                   self.particle_system.x_dimension)
        assert means.shape == (self.n_times, self.particle_system.x_dimension)
        assert covariances.shape == (self.n_times,
                                     self.particle_system.x_dimension,
                                     self.particle_system.x_dimension)
        assert numpy.abs(means - self.x).max() < 3
        assert numpy.abs(covariances).max() < 2.5
        assert -50 < log_likelihood < -20


# --------------------------------
# Local Variables:
# mode: python
# End:
