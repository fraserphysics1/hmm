"""test_simple.py Tests hmm.simple using pytest

hmm.simple.Observation is tested with hmm.base.IntegerObservation in
test_base.py

$ pytest src/hmm/tests/test_simple.py
  or
$ pytest --pdb src/hmm/tests/test_simple.py

"""
# Uses the test fixture "models" that is defined in conftest.py

import types

import numpy
import numpy.testing
import numpy.random

import pytest
import scipy.linalg  # type: ignore

import hmm.simple


# pylint: disable = missing-function-docstring
def test_state_simulate(models):
    result1 = models.simple_hmm.state_simulate(models.n_times)
    result2 = models.simple_hmm.state_simulate(models.n_times, models.mask)
    for result in (result1, result2):
        assert len(result) == models.n_times
        array = numpy.array(result)
        assert array.min() == 0
        assert array.max() == models.n_states - 1


def test_link(models):
    """ Remove link from 0 to itself
    """
    models.simple_hmm.link(0, 0, 0)
    assert models.simple_hmm.p_state2state[0, 0] == 0.0


def test_str(models):
    string = str(models.simple_hmm)
    assert string.find('with 6 states') == 25


def test_unseeded(models):
    """ Exercise initialization of HMM without supplying rng
    """

    mod = hmm.simple.HMM(
        models.simple_hmm.p_state_initial.copy(
        ),  # Initial distribution of states
        models.simple_hmm.p_state_time_average.copy(
        ),  # Stationary distribution of states
        models.simple_hmm.p_state2state.copy(
        ),  # State transition probabilities
        models.simple_hmm.y_mod,
    )
    states, y = mod.simulate(10)  # pylint: disable = unused-variable
    assert len(states) == 10


def test_decode(models):
    """
    Check that mod gets 70% of the states right
    """

    states = models.simple_hmm.decode(models.observations)
    wrong = numpy.where(states != models.states)[0]
    assert len(wrong) < len(states) * .3


def test_train(models):
    """ Test training
    """

    py_state = models.simple_hmm.y_mod._py_state.copy()
    p_state2state = models.simple_hmm.p_state2state.copy()
    log_like = models.simple_hmm.train(models.observations,
                                       n_iterations=10,
                                       display=True)
    # Check that log likelihood increases montonically
    for i in range(1, len(log_like)):
        assert log_like[i - 1] < log_like[i]
    # Check that trained model is close to true model
    numpy.testing.assert_allclose(models.simple_hmm.y_mod._py_state.values(),
                                  py_state,
                                  atol=0.15)
    numpy.testing.assert_allclose(models.simple_hmm.p_state2state.values(),
                                  p_state2state,
                                  atol=0.2)


def test_strip(models):
    models.simple_hmm.strip()


def test_calculate(models):
    models.simple_hmm.y_mod.observe(models.observations)
    models.simple_hmm.calculate()


@pytest.fixture(scope="function")
def probabilities():
    """Make objects used by tests.

    """

    A_ = numpy.array([[0, 2, 2.0], [2, 2, 4.0], [6, 2, 2.0]])
    B_ = numpy.array([[0, 1], [1, 1], [1, 3.0]])
    C_ = numpy.array([[0, 0, 2.0], [0, 0, 1.0], [6, 0, 0.0]])

    # The tests of step_forward and step_back use prob_a
    prob_a = hmm.simple.Prob(A_.copy())
    # likelihood_s is for testing utility()
    likelihood_s = hmm.simple.Prob(B_.copy())
    prob_c = hmm.simple.Prob(C_.copy())
    probs = (prob_a, likelihood_s, prob_c)
    for x in probs:
        x.normalize()
    return types.SimpleNamespace(prob_a=prob_a,
                                 prob_c=prob_c,
                                 likelihood_s=likelihood_s,
                                 probs=probs)


def test_normalize(probabilities):
    for x in probabilities.probs:
        n_rows, n_columns = x.shape
        for i in range(n_rows):
            total = 0
            for j in range(n_columns):
                total += x.values()[i, j]
            numpy.testing.assert_allclose(1, total)


def test_assign(probabilities):
    total = probabilities.prob_c.values().sum()
    probabilities.prob_c.assign_col(1, [1, 1, 1])
    numpy.testing.assert_allclose(probabilities.prob_c.values().sum(),
                                  total + 3)


def test_likelihoods(probabilities):
    numpy.testing.assert_allclose(
        probabilities.prob_c.likelihoods([0, 1, 2])[2], [1, 1, 0])


def test_utility(probabilities):
    numpy.testing.assert_allclose(
        probabilities.prob_c.utility(probabilities.likelihood_s.T[0],
                                     probabilities.likelihood_s.T[1]),
        [[0, 0, 0], [0, 0, 0.375], [0.25, 0, 0]])


def test_step_forward(probabilities):
    alpha = numpy.array([1., 0.5, 0.75])
    probabilities.prob_a.step_forward(alpha)
    numpy.testing.assert_allclose(alpha, [0.575, 0.775, 0.9])


def test_step_back(probabilities):
    beta = numpy.array([1., 0.5, 0.75])
    probabilities.prob_a.step_back(beta)
    numpy.testing.assert_allclose(beta, [0.625, 0.75, 0.85])


def test_values(probabilities):
    numpy.testing.assert_allclose(probabilities.prob_c.values(),
                                  [[0, 0, 1], [0, 0, 1], [1, 0, 0]])


# --------------------------------
# Local Variables:
# mode: python
# End:
