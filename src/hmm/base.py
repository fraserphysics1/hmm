"""base.py: Versions of code in simple.py that are extendable in ways
necessary for some applications such as working with multiple segments
of data.

"""

from __future__ import annotations  # Enables, eg, (self: HMM,

import typing  # For type hints

import numpy
import numpy.random

import hmm.simple


class BaseObservation:
    """Base class for observations.  You can't use instances of this
    class.  You must use a subclass.

    """
    _parameter_keys: typing.List[str] = ['n_states']

    # Specifies parameters reported by self.__str__.  These parameter
    # names should be in self.__dict__.

    def __init__(self: BaseObservation, *args):
        self._rng = args[-1]
        self.n_states = 0  # Subclasses must set n_states
        for key in self._parameter_keys:
            assert key in self.__dict__

    def _concatenate(self: BaseObservation, segments: typing.Sequence,
                     lengths) -> tuple:
        """Concatenate observation segments.  Assumes each is a numpy array.

        Args:
            segments:  The observations.  The structure depends on the subclass
            lengths: Truncate each returned segment[i] to lengths[i]

        Returns: (tuple): (y, t_seg) where the structure of y depends
            on the subclass, and t_seg is a list of segment
            boundaries.

        """
        assert isinstance(segments, (tuple, list))
        if lengths is None:
            lengths = numpy.array(list(len(segment) for segment in segments))
        if lengths.min() < 1:
            raise ValueError(f'{lengths.min()=} is < 1. {lengths=}')
        t_seg = numpy.zeros(len(lengths) + 1, dtype=int)
        t_seg[1:] = lengths.cumsum()
        all_data = numpy.concatenate(
            list(
                segment[:length] for segment, length in zip(segments, lengths)))
        return all_data, t_seg

    def reestimate(self: BaseObservation, weights: numpy.ndarray):
        """Based on weight, modify parameters of self

        Args:
            weight (numpy.ndarray):  A statistic of all the data, y, and old
                parameters of HMM including self.
                weight[t, s] = Prob(state[t]=s|y and HMM)

        Sets parameters of self to \argmax_\theta Prob(y|w) (Todo:
        verify)

        """
        raise NotImplementedError('Not implemented.  Use a subclass.')

    def random_out(self: BaseObservation, state: int):
        """ Returns a random draw from Prob(y|state)

        Args:
            state: The state

        Returns:
            (object): A single observation

        """
        raise NotImplementedError('Not implemented.  Use a subclass.')

    def calculate(self: BaseObservation) -> numpy.ndarray:
        """Calculate and return the likelihoods of states given observations.

        Returns:
            self._likelihood where _likelihood[t,i] = Prob(y[t]|state[t]=i)

        self.observe must be called first to assign measured data to self.
        """
        raise NotImplementedError('Not implemented.  Use a subclass.')

    def debug_impossible_data(self: BaseObservation, msg=''):
        """Call from calculate method of subclass

        """
        if self._likelihood.min() < 0:
            raise ArithmeticError(f'negative likelihood {msg}')
        likelihood_t = self._likelihood.sum(axis=1)
        if likelihood_t.min() <= 0.0:
            bad_times = numpy.nonzero(likelihood_t <= 0.0)
            try:
                bad_data = self._y[bad_times]
            except:
                bad_data = 'self._y[bad_times] fails'
            raise ArithmeticError(f'''impossible data at times:
{bad_times}
data:
{bad_data}
{msg}''')

    def merge(self: BaseObservation, raw_outs) -> numpy.ndarray:  # pylint: disable=no-self-use
        """Reformat raw_outs into suitable form for self.observe()

        This idempotent, ie, it's OK to call it twice on the same data.

        """
        # Motivating example: raw_outs is a list of JointSegment
        # instances from JointObservation.random_out().

        if isinstance(raw_outs, numpy.ndarray):
            return raw_outs
        return numpy.array(raw_outs).reshape(-1)

    def observe(self: BaseObservation, y_segs, lengths=None) -> numpy.ndarray:
        """Attach observations to self

        Args:
            y_segs: Independent measurement sequences.  Structure
                specified by implementation of self._concatenate() in
                subclasses.
            lengths: Only use lengths[i] of y_segs[i]

        Returns:
            t_segs: Segment boundaries

        """
        # pylint: disable = attribute-defined-outside-init
        self._y, t_seg = self._concatenate(y_segs, lengths)
        self.t_seg = numpy.array(t_seg)
        self.n_times = t_seg[-1]
        self._likelihood = numpy.empty((self.n_times, self.n_states))
        return self.t_seg

    def __str__(self: BaseObservation) -> str:
        """Return a string representation of self.

        Returns:
            A string representation of self

        """
        return_string = f'An {type(self)} instance:\n'
        for key in self._parameter_keys:
            return_string += f'    {key}\n'
            return_string += f'{getattr(self, key)}\n'
        return return_string

    def strip(self: BaseObservation):
        """Free memory
        """

        self._likelihood = None
        self._y = None


class IntegerObservation(BaseObservation):
    r"""Observation model for integers with y[t] \in [0:y_max] \forall t

    Args:
        py_state: Conditional probability of each y give each state
        rng: A numpy.random.Generator for simulation

    Keyword Args:
        warn: If True, print warnings

    """

    _parameter_keys = ['_py_state']

    def __init__(self: IntegerObservation,
                 py_state: numpy.ndarray,
                 rng: numpy.random.Generator,
                 warn: typing.Optional[bool] = True):
        self._py_state = hmm.simple.Prob(py_state)
        super().__init__(rng)
        self.warn = warn

        self._cummulative_y = numpy.cumsum(self._py_state, axis=1)
        self.n_states = len(self._py_state)

    def reestimate(  # pylint: disable = arguments-differ
            self: IntegerObservation, weights: numpy.ndarray):
        """
        Estimate new model parameters.

        Args:
            weights: weights[t,s] = Prob(state[t]=s) given data and
                 old model
        """
        if not (isinstance(self._y, numpy.ndarray) and
                (self._y.dtype == numpy.int32)):
            # pylint: disable = attribute-defined-outside-init
            self._y = numpy.array(self._y, numpy.int32)
            if self.warn:
                print("Warning: reformatted y in reestimate")
        assert self._y.dtype == numpy.int32 and self._y.shape == (
            self.n_times,), f"""
                y.dtype={self._y.dtype}, y.shape={self._y.shape}"""
        for y_i in range(self._py_state.shape[1]):
            self._py_state.assign_col(
                y_i,
                weights.take(numpy.where(self._y == y_i)[0],
                             axis=0).sum(axis=0))
        self._py_state.normalize()
        self._cummulative_y = numpy.cumsum(self._py_state, axis=1)

    def calculate(self: IntegerObservation) -> numpy.ndarray:
        r"""
        Calculate likelihoods: self._likelihood[t,i] = P(y(t)|state(t)=i)

        Returns:
            state_likelihood[t,i] \forall t \in [0:n_times] and i \in [0:n_states]

        Assumes a previous call to observe has assigned self._y and allocated
            self._likelihood

        """

        # mypy objects ""Unsupported target for indexed assignment"
        self._likelihood[:, :] = self._py_state[:, self._y].T  # type: ignore
        self.debug_impossible_data()
        return self._likelihood

    def random_out(self: IntegerObservation, state: int) -> int:
        """For simulation, draw a random observation given state s

        Args:
            state: Index of state

        Returns: Random observation drawn from distribution
            conditioned on the state

        """
        return numpy.searchsorted(self._cummulative_y[state],
                                  self._rng.random())


class ClassObservation(IntegerObservation):

    _parameter_keys = "n_class n_states class2state ".split()

    def __init__(self: ClassObservation, class2state: dict):
        r"""Output of state is the class it's in.  Used for supervised
        training.

        Args:
            class2state: Keys are integer classes.  Values are lists of integer
                         indices of states

        """
        self.class2state = class2state
        self.n_class = len(self.class2state)
        # Find out how many states there are and ensure that each
        # state is in only one class.
        states: typing.Set[int] = set()
        for x in self.class2state.values():
            # Assert that no state is in more than one class.
            assert states & set(x) == set()
            states = states | set(x)
        self.n_states = len(states)
        # Ensure that states is a set of sequential integers [0,n_states)
        assert states == set(range(self.n_states))

        # class_and_state[class_id, state_id] is true iff state \in class.
        self.class_and_state = numpy.zeros((self.n_class, self.n_states), bool)

        # state2class[state_id] = class_id for class that contains state
        self.state2class = numpy.ones(self.n_states, dtype=numpy.int32) * -1

        for class_id, states in self.class2state.items():
            for state_id in states:
                self.class_and_state[class_id, state_id] = True
                # Ensure that no state is in more than one class
                assert self.state2class[state_id] == -1
                self.state2class[state_id] = class_id

    def random_out(self: ClassObservation, state_id: int) -> int:
        return self.state2class[state_id]

    def calculate(self: ClassObservation) -> numpy.ndarray:
        """Return likelihoods of states given self._y, a sequence of
        classes.

        """
        self._likelihood *= 0.0
        for t, _class in enumerate(self._y):
            self._likelihood[t, self.class2state[_class]] = 1.0
        self.debug_impossible_data()
        return self._likelihood

    def reestimate(self: ClassObservation, w: numpy.ndarray):
        pass


class BadObservation(ClassObservation):

    _parameter_keys = "n_class n_states class2state _py_state".split()

    def __init__(self: BadObservation, class2state: dict, bad2class: dict):
        r"""Includes bad states that can emit any of several classes.

        Args:
            class2state: Keys are integer classes.  Values are lists of integer
                         indices of states
            bad2class: Keys are integer indices of states.  Values are lists of
                       pairs (class, probability)

        Normal states emit the class specified by class2state with
        probability 1.0.  Bad states emit classes with probability
        specified by the bad2class argument.  Like parent,
        ClassObservation, the parameters of this class are not
        modified by training.  Use this observation class for training
        data most of which has class information but also includes bad
        or missing data points that don't have class information.

        """
        self.class2state = class2state
        self.n_class = len(self.class2state)
        # Find out how many states there are and ensure that each
        # state is in only one class.
        states: typing.Set[int] = set()
        for x in self.class2state.values():
            # Assert that no state is in more than one class.
            assert states & set(x) == set()
            states = states | set(x)
        self.n_states = len(states)

        # Ensure that states is a set of sequential integers [0,n_states)
        assert states == set(range(self.n_states))

        self.state2class = numpy.ones(self.n_states, dtype=numpy.int32) * -1
        self._py_state = hmm.simple.Prob(
            numpy.zeros((self.n_states, self.n_class)))

        # Process the normal good classes
        for class_index, states in self.class2state.items():
            for state_index in states:
                self.state2class[state_index] = class_index
                self._py_state[state_index, class_index] = 1.0

        # Process the bad classes
        for state_index, pair_list in bad2class.items():
            self._py_state[state_index, :] *= 0.0
            for class_index, probability in pair_list:
                self._py_state[state_index, class_index] = probability
        self._py_state.normalize()

    def calculate(self: BadObservation):
        """Calculate self._likelihood[t,i] = P(self._y[t]|state[t] = i)

        """
        return IntegerObservation.calculate(self)


class HMM(hmm.simple.HMM):
    """Uses observations that are in many segments.

    """
    # Allow y_mod type to conflict with hmm.simple.HMM
    y_mod: BaseObservation  # type: ignore

    def reestimate(self: HMM):
        """Phase of Baum Welch training that reestimates model parameters

        Calculate new model parmeters given the quantities already
        calculated, ie, self.state_likelihood, self.alpha, self.beta,
        and self.gamma_inv, and Py, these calcuations are independent
        of the observation model calculations.  This delegates
        reestimating observation model parameter to a call
        self.y_mod.reestimate()

        """

        # u_sum[i,j] = \sum_{t:gamma_inv[t+1]>0} alpha[t,i] * beta[t+1,j] *
        # state_likelihood[t+1,j]/gamma[t+1]
        #
        # The term at t is the conditional probability that there was
        # a transition from state i to state j at time t given all of
        # the observed data
        u_sum = numpy.einsum(
            "ti,tj,tj,t->ij",  # Specifies the i,j indices and sum over t
            self.alpha[:-1],  # indices t,i
            self.beta[1:],  # indices t,j
            self.state_likelihood[1:],  # indices t,j
            self.gamma_inv[1:]  # index t
        )
        # gamma_inv[t] == 0 at the beginning of each segment.  That
        # makes the contributions to u_sum across boundaries zero.

        self.alpha *= self.beta  # Saves allocating a new array for
        alpha_beta = self.alpha  # the result

        self.p_state_time_average = alpha_beta.sum(axis=0)  # type: ignore
        self.p_state_initial = numpy.copy(alpha_beta[0])
        for x in (self.p_state_time_average, self.p_state_initial):
            x /= x.sum()
        assert u_sum.shape == self.p_state2state.shape
        self.p_state2state *= u_sum
        self.p_state2state.normalize()
        self.y_mod.reestimate(alpha_beta)

    def forward(  # pylint: disable = arguments-differ
            self: HMM,
            t_start: int = 0,
            t_stop: int = 0,
            t_skip: int = 0,
            last_0=None) -> float:
        """Recursively calculate state probabilities, P(s[t]|y[0:t])

        Args:
            t_start: Use self.state_likelihood[t_start] first
            t_stop: Use self.state_likelihood[t_stop-1] last
            t_skip: Number of time steps from when "last" is valid till t_start
            last_0: Optional initial distribution of states

        Returns:
            Log (base e) likelihood of HMM given entire observation sequence

        Works on a single uninterrupted sequence of observations that
        may be only part of a larger set.  As side effect, it sets
        self.alpha and self.gamma_inv for the same range of t as the
        observations.

        """
        if t_stop == 0:
            # Reduces to ignoring t_start and t_stop and operating on
            # a single segment
            assert t_start == 0
            t_stop = len(self.state_likelihood)

        if last_0 is None:
            last_0 = self.p_state_initial

        last = numpy.copy(last_0).reshape(-1)
        for t in range(t_skip):
            # The following replaces last in place with last *
            # p_state2state ** t_skip
            self.p_state2state.step_forward(last)

        for t in range(t_start, t_stop):
            last *= self.state_likelihood[t]  # Element-wise multiply
            assert last.sum(
            ) > 0, f'Impossible data {t=} {t_start=} {t_stop=} {self.state_likelihood[t].sum()=}'
            self.gamma_inv[t] = 1 / last.sum()
            last *= self.gamma_inv[t]
            self.alpha[t, :] = last
            self.p_state2state.step_forward(last)
        return -(numpy.log(self.gamma_inv[t_start:t_stop])).sum()

    def backward(  # pylint: disable = arguments-differ
            self: HMM, t_start=0, t_stop=0):
        """
        Baum Welch backwards pass through state conditional likelihoods.


        Calculates values of self.beta which "reestimate()" needs.
        """

        # FixMe; What about t_skip?
        if t_stop == 0:
            # Reduces to ignoring t_start and t_stop and operating on
            # a single segment
            assert t_start == 0
            t_stop = len(self.state_likelihood)

        last = numpy.ones(self.n_states)
        for t in range(t_stop - 1, t_start - 1, -1):
            self.beta[t, :] = last
            last *= self.state_likelihood[t] * self.gamma_inv[t]
            self.p_state2state.step_back(last)

    def multi_train(self: HMM, y_sequences, n_iterations: int, display=True):
        """Train on more than one independent sequence of observations

        Args:
            y_sequences: Measured observation sequences in format appropriate
                for self.y_mod
            n_iterations: The number of iterations to execute

        Returns:
            List of utility (log likelihood or log MAP) / n_observation
            for each iteration

        Generalizes simple.HMM by allowing more than one independent
        observation sequence.  Also accommodates MAP estimation
        instead of MLE.

        For the first training iteration, the initial distribution of
        states at the beginning of each observation sequence is given
        by self.p_state_initial.  However, each training iteration
        updates the distribution of the initial state for each
        observation sequence independently.  At the end of training,
        the average of those independent distributions for the initial
        states is assigned to self.p_state_initial.

        I don't know of assumptions for which that procedure is
        exactly correct.  If one knows that each observation segment
        starts with the same state distribution, then one should use a
        single initial state distribution in training.  If one knows
        that the initial state distributions are different and that
        when the model is used it will be applied to new data drawn
        from similar sets of segments, then one should train with
        separate initial state distributions and retain them for use
        in the future.

        The compromise treatment of the initial distribution of states
        here is intended to treat initial distributions that are
        different from each other but more similar to each other than
        the time averaged state distribution.

        """

        # pylint: disable = attribute-defined-outside-init

        # utility_list[i] = log(Prob(y_sequences|HMM[iteration=i]))/n_times, ie,
        # the log likelihood per time step, or MAP
        utility_list = []

        t_seg = self.y_mod.observe(y_sequences)  # Segment boundaries
        self.n_times = self.y_mod.n_times

        assert self.n_times > 1
        assert t_seg[0] == 0

        n_seg = len(t_seg) - 1
        self.alpha = numpy.empty((self.n_times, self.n_states))
        self.beta = numpy.empty((self.n_times, self.n_states))
        self.gamma_inv = numpy.empty((self.n_times,))

        # State probabilities at the beginning of each segment
        p_state_initial_all = numpy.empty((n_seg, self.n_states))
        for seg in range(n_seg):
            p_state_initial_all[seg, :] = self.p_state_initial.copy()

        for iteration in range(n_iterations):
            message = [f"i={iteration:4d} "]
            sum_log_like = 0.0
            self.state_likelihood = self.y_mod.calculate()

            # Operate on each observation segment separately and put
            # the results in the corresponding segement of the alpha,
            # beta and gamma_inv arrays.

            for seg in range(n_seg):
                # Set up self to run forward and backward on this segment
                t_start = t_seg[seg]
                t_stop = t_seg[seg + 1]

                log_likelihood = self.forward(
                    t_start, t_stop, last_0=p_state_initial_all[seg, :])
                self.backward(t_start, t_stop)

                sum_log_like += log_likelihood
                p_state_initial_all[
                    seg, :] = self.alpha[t_start] * self.beta[t_start]
                # Flag to prevent fitting state transitions between segments
                self.gamma_inv[t_start] = 0
                message.append(f"L[{seg}]={(log_likelihood):7.4f} ")

            # Record/report/check this iteration
            if hasattr(self.y_mod, 'log_prior'):
                temp = self.y_mod.log_prior()
                utility = sum_log_like + temp
                message.append(f"L prior {temp:10.3e} ")
            else:
                utility = sum_log_like
            utility_list.append(utility / self.n_times)
            message.append(f"U/n={utility_list[-1]:10.7f}")
            self.ensure_monotonic(utility_list, display, ''.join(message))

            self.reestimate()

        self.p_state_initial[:] = p_state_initial_all.sum(axis=0)
        self.p_state_initial /= self.p_state_initial.sum()
        return utility_list

    def initialize_y_model(
            self: HMM,
            y,
            state_sequence: typing.Optional[numpy.ndarray] = None):
        """Given data, make a plausible y_model.

        Args:
            y: Observation sequence.  Type must work for self.y_mod.observe(y)
            state_sequence: Optional state sequence

        Use this method to create an observation model that is not the
        same for every state and that makes the data in the argument y
        plausible.

        """
        # ToDo: Fails for general y_mod
        n_times = self.y_mod.observe(y)[-1]
        if state_sequence is None:
            state_sequence = numpy.array(self.state_simulate(n_times),
                                         numpy.int32)

        # Set alpha to enforce the simulated state sequence in reestimate
        alpha = numpy.zeros((n_times, self.n_states))
        for t in range(n_times):
            alpha[t, state_sequence[t]] = 1
        self.y_mod.reestimate(alpha)
        return self.y_mod


class JointSegment(dict):
    """A sequence of observations with components of different types

    Args:
        input_: Either a dict of sequences or a sequence of dicts.

    Notes: Ints are not allowed as keys of Joint Segments.

    Lengths must match if allow_lengths is False

    """

    def __init__(self: JointSegment, input_, allow_lengths=True):
        if isinstance(input_, dict):
            observation_dict = input_
            keys = list(observation_dict.keys())
        else:
            # Assume arg is a time series of dicts like HMM.simulate
            # returns
            keys = list(input_[0].keys())
            lists = dict((key, []) for key in keys)
            for dict_ in input_:
                for key in keys:
                    lists[key].append(dict_[key])
            observation_dict = dict((key, lists[key]) for key in keys)
        super().__init__(observation_dict)
        self._length = len(self[keys[0]])
        for key, value in self.items():
            assert allow_lengths or len(
                value
            ) == self._length, f"len(self[{keys[0]}]) = {len(self[keys[0]])} != {len(value)} = len(self[{key}])"
            # Integers should not be keys because if 5 were a key,
            # segment[5] would yield the fifth item in the sequence of
            # observations, but for any other type of key segment[key]
            # to yields a component sequence.
            if isinstance(key, int):
                raise TypeError("Don't use int as key in JointSegment")

    def __len__(self: JointSegment) -> int:
        return self._length

    def __str__(self: JointSegment) -> str:
        result = [''.join([f"{key:9s}" for key in self.keys()])]
        for t in range(len(self)):
            result.append(
                [f"{self[key][t].__str__():9s}" for key in self.keys()])
        return ''.join(result)

    def __getitem__(self: JointSegment, val) -> JointSegment:
        if not type(val) in (int, slice):
            return dict.get(self, val)
        new_dict = dict((key, value[val]) for key, value in self.items())
        return self.__class__(new_dict)


class JointObservation(BaseObservation, dict):
    """Observation model for data that is JointSegment instances

    Args:
        model_dict: A dict of models.
        power: A dict of exponential weights

    Note: Keys of model_dict must match keys of the JointSegment data.
    
    """

    # pylint: disable = super-init-not-called
    def __init__(self: JointObservation, model_dict: dict, power=None):
        dict.__init__(self, model_dict)
        self.first_key = next(iter(model_dict))
        self.n_states = self[self.first_key].n_states
        self.power = power
        for key, model in self.items():
            assert isinstance(model, BaseObservation), f'{key=} {type(model)=}'
            assert model.n_states == self.n_states, f'{self.n_states=} {key=} {model.n_states=}'

    def __str__(self):
        result = [f'{self.__class__} instance\n']
        for key, value in self.items():
            result.append(f'    {key}: {value}\n')
        return ''.join(result)

    def log_prior(self):
        result = 0.0
        for model in self.values():
            if hasattr(model, "log_prior"):
                result += model.log_prior()
        return result

    def observe(  # pylint: disable = arguments-renamed
            self: JointObservation,
            segment_list: list,
            lengths=None) -> numpy.ndarray:
        """Attach observations to self as a single JointSegment

        Args:
            segment_list: List of JointSegment instances

        Side effects: 1. Assign self.t_seg; 2. Call observe method of
        sub-models.  3. Truncate components to make segments match.

        Note: Because the observe methods of AR models drop the first
        ar_order observations of each segment, one must take care to
        ensure that the elements of the segment_list argument will be
        aligned in time.  Consider padding the begnning of each AR
        segment with ar_order fictitious observations.

        """
        key_list = list(self.keys())
        key_set = set(key_list)

        # Collect all segments for all keys
        segments_in = dict((key, []) for key in key_list)
        for segment in segment_list:
            for key in key_list:
                segments_in[key].append(segment[key])

            assert isinstance(segment, JointSegment), f'{type(segment)=}'
            assert set(segment.keys()
                      ) == key_set, f'''Data keys don't match model keys:
{segment.keys()} from data segment
{key_set=} from model'''

        # Calculate min_lengths, the shortest elements of each input segment
        seg_lengths = numpy.zeros((len(key_list), len(segment_list)), dtype=int)
        for i_key, t_seg_key in enumerate(
                # First observe pass is only for getting lengths of
                # segments.  The lengths are modified for AR model.
                self[key].observe(segments_in[key]) for key in key_list):
            t_seg = numpy.array(t_seg_key)
            seg_lengths[i_key, :] = t_seg[1:] - t_seg[:-1]
        min_lengths = seg_lengths.min(axis=0)
        assert len(min_lengths) == len(segment_list)

        observation_dict = {}
        for key in key_list:

            # Concatenate segments_in[key] and assign self[key]._y
            t_seg = self[key].observe(segments_in[key], lengths=min_lengths)
            # pylint: disable = protected-access
            observation_dict[key] = self[key]._y
            assert min_lengths.sum() == t_seg[-1]
            assert len(self[key]._y) == t_seg[-1]

        # pylint: disable = attribute-defined-outside-init
        self.t_seg = self[self.first_key].t_seg

        for key in key_list:
            if not numpy.array_equal(self.t_seg, self[key].t_seg):
                raise ValueError(f"Observations for {key} are not aligned")

        self._y = JointSegment(observation_dict)
        self.n_times = self.t_seg[-1]
        return self.t_seg

    def random_out(self: JointObservation, state: int) -> JointSegment:
        """ Draw a single output from the conditional distribution given state.
        """
        observation_dict = {}
        for key, model in self.items():
            observation_dict[key] = [model.random_out(state)]
        return JointSegment(observation_dict)

    def calculate(self: JointObservation) -> numpy.ndarray:
        """Calculate and return likelihoods of states given self._y.

        Returns:
            likelihood with likelihood[t,s] = Probability(y[t]|state[t]=s)

        Requires self._y to have been assigned by a call to
        self.observe().  Since the components of self._y[t] are
        conditionally independent given state[t], the likeliyhood of
        the joint observation is the product of the likelihoods of the
        components.

        """
        # pylint: disable = attribute-defined-outside-init
        if self.power:
            for key, model in self.items():
                if key == self.first_key:
                    self._likelihood = model.calculate().copy()**self.power[key]
                else:
                    self._likelihood *= model.calculate()**self.power[key]
                model.debug_impossible_data(f'{key=} {model=}')
        else:
            for key, model in self.items():
                if key == self.first_key:
                    self._likelihood = model.calculate().copy()
                else:
                    self._likelihood *= model.calculate()
                model.debug_impossible_data(f'{key=} {model=}')

        self.debug_impossible_data()
        return self._likelihood

    def reestimate(self: JointObservation, w: numpy.ndarray):  # pylint: disable=arguments-renamed
        """Reestimate parameters of self.y_mod

        Args:
            w: Weights with w[t,s] = alpha[t,s]*beta[t,s] =
                Probability(state=s|all data)

        Assumes that observations are already attached to self.y_mod by
        self.observe().

        """
        for model in self.values():
            model.reestimate(w)

    def strip(self: JointObservation):
        """Remove observations before writing to disk

        """
        for model in self.values():
            model.strip()
        super().strip()

    def merge(self: JointObservation, raw_outs: list) -> JointSegment:
        if isinstance(raw_outs, JointSegment):
            return raw_outs
        # Assume raw_outs is a list of JointObservation instances and
        # the keys of each instance match the keys of self.
        unmerged = JointSegment(raw_outs)
        for key in self.keys():
            unmerged[key] = self[key].merge(unmerged[key])
        return unmerged  # 'cause it's merged now
