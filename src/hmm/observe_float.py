"""observe_float.py: Various observation models for floating point measurements.

Use these models with the Hidden Markov Models in base.py.

"""

# Copyright 2021-2022 Andrew M. Fraser

# This file is part of hmm.

# Hmm is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.

# I've lost track of where I got the MAP formulas.  I've used
# wikipedia (https://en.wikipedia.org/wiki/Conjugate_prior) to check
# them.  Wikipedia cites
# https://www.cs.ubc.ca/~murphyk/Papers/bayesGauss.pdf which is
# incomplete and out of date.

# pylint: disable = attribute-defined-outside-init

from __future__ import annotations  # Enables, eg, (self: HMM,

import typing

import numpy

import hmm.base


class Gauss(hmm.base.IntegerObservation):
    r"""Scalar Gaussian observation model

    Args:
        mu: numpy.ndarray Means; a value for each state
        variance: numpy.ndarray Variances; a value for each state.
        rng: Generator with state

    The probability of observation y given state s is:
    Prob(y|s) = 1/\sqrt(2\pi var[s]) exp(-(y-mu[s])^2/(2*var[s])

    """
    _parameter_keys = "mu variance".split()

    def __init__(  # pylint: disable = super-init-not-called
        self: Gauss,
        mu: numpy.ndarray,
        variance: numpy.ndarray,
        rng: numpy.random.Generator,
    ):
        assert len(variance) == len(mu)
        self.mu = mu
        self.variance = variance
        self.sigma = numpy.sqrt(variance)
        self._rng = rng
        self.dtype = [numpy.float64]
        self.norm = 1 / numpy.sqrt(2 * numpy.pi * variance)
        self.n_states = len(variance)

    # Ignore: Super returned int
    def random_out(  # type: ignore
            self: Gauss, state: int) -> float:
        """ For simulation, draw a random observation given state s

        Args:
            state: Index of state

        Returns:
            Random observation drawn from distribution conditioned on state s

        """
        return self._rng.normal(self.mu[state], self.sigma[state])

    def calculate(self: Gauss,) -> numpy.ndarray:
        """Calculate and return likelihoods of states for all observations.

        Returns:
            self.p_y with self.p_y[t,s] = Prob(y[t]|state[t]=s)

        """
        assert self._y.reshape((-1, 1)).shape == (self.n_times, 1)
        diff = self.mu - self._y.reshape((-1, 1))
        self._likelihood = numpy.exp(-diff * diff /
                                     (2 * self.variance)) * self.norm
        self.debug_impossible_data()
        return self._likelihood

    # ToDo: Why do I need Any to make mypy happy?
    def diffs_to_var(  # pylint: disable=no-self-use
            self: Gauss, diffs: numpy.ndarray, wsum: typing.Any) -> typing.Any:
        """ Formula for reestimating variance.

        Args:
            diffs: Array of weighted differences, observation - mu
            wsum: Sum of weights

        Returns:
            New estimate of variance

        Not in-line for convenience of MAP subclass
        """
        return (diffs * diffs).sum(axis=0) / wsum

    # Ignore: Super has optional argument warn
    def reestimate(  # type: ignore
            self: Gauss, weights: numpy.ndarray):
        """
        Estimate new model parameters.  self._y already assigned

        Args:
            weights: weights[t,s] = Prob(state[t]=s) given data and
                old model

        """
        assert len(self._y) > 0
        y = self._y
        assert weights.shape == (len(y), self.n_states)
        wsum = weights.sum(axis=0)
        self.mu = (weights.T * y).sum(axis=1) / wsum
        diffs = (self.mu - y.reshape((-1, 1))) * numpy.sqrt(weights)
        self.variance = self.diffs_to_var(diffs, wsum)
        self.sigma = numpy.sqrt(self.variance)
        self.norm = 1 / numpy.sqrt(2 * numpy.pi * self.variance)


class GaussMAP(Gauss):
    """Use maximum a posteriori probability estimation for variance.
    
    Args:
        mu: Means of the states
        variance: Variances of states
        rng: A numpy random generator

    Keyword Args:
        alpha: Shape parameter of inverse gamma prior for variance
        beta: Scale parameter of inverse gamma prior for variance

    The Normal-gamma is a conjgate prior for both the mean and
    variance.  It's uninformative for the mean in the limit lambda ->
    0, and in that limit the prior for T, the precision or inverse
    variance, is proportional a gamma prior for T which is used when
    the mean is known.  Here, rather than have a prior for the
    precision, I'm using inverse gamma prior for the variance, which
    is appropriate if the mean is known.

    """

    # From https://en.wikipedia.org/wiki/Inverse-gamma_distribution
    # f(x; \alpha, \beta)
    # = \frac{\beta^\alpha}{\Gamma(\alpha)}
    # (1/x)^{\alpha + 1}\exp\left(-\beta/x\right)

    def _set_alpha_beta(self, alpha, beta):
        """Called by __init__ of GaussMAP and """
        if isinstance(alpha, float):
            self.alpha = numpy.ones(self.n_states) * alpha
        else:
            assert alpha.shape == (self.n_states,)
            self.alpha = alpha
        if isinstance(beta, float):
            self.beta = numpy.ones(self.n_states) * beta
        else:
            assert beta.shape == (self.n_states,)
            self.beta = beta

    def __init__(
        self: GaussMAP,
        mu: numpy.ndarray,
        variance: numpy.ndarray,
        rng: numpy.random.Generator,
        alpha: typing.Union[float, numpy.ndarray] = 1.0,
        beta: typing.Union[float, numpy.ndarray] = 1.0,
    ):
        super().__init__(mu, variance, rng)
        self._set_alpha_beta(alpha, beta)

    def log_prior(self: GaussMAP) -> float:
        r"""Calculate (discarding constant terms) the log probability of the prior.

        f(x;\alpha,\beta) \propto x^{-\alpha-1} e^{-\beta/x}
        log f(x;\alpha,\beta) = c - (\alpha+1)\log(x) -\beta/x
        """
        return_value = 0.0
        for state in range(self.n_states):
            return_value += -(self.alpha[state] + 1) * numpy.log(
                self.variance[state]) - self.beta[state] / self.variance[state]
        return return_value

    def diffs_to_var(self: GaussMAP, diffs: numpy.ndarray, wsum: float):
        """Calculate maximum a posteriori probability variance.

        Args:
            diffs: diffs[t,s] = sqrt(weights[t,s])*(y[t] - mu[t,s])
            wsum: wsum[s] = sum_t weights[t,s]
        """
        numerator = 2 * self.beta + (diffs * diffs).sum(axis=0)
        denominator = 2 * self.alpha + 2 + wsum
        return numerator / denominator


class MultivariateGaussian(hmm.base.BaseObservation):
    """Observation model for vector measurements.

    Args:
        mu[n_states, dimension]: Mean of distribution for each state
        sigma[n_states, dimension, dimension]: Covariance matrix for each state
        rng: Random number generator with state

    Keyword Args:
        Psi: Inverse Wishart scale (a float, one matrix, or a matrix for each state)
        nu: Inverse Wishart degrees of freedom (one float or a float for each state)
        small: Raise error if total likelihood of any observation is
            less than small

    Implements Maximum a posteriori probability estimation of the
    variance of each state.  Without data, sigma[s,i,i] =
    \frac{Psi}{nu + dim +1} where dim is the dimension of the
    observations

    """
    _parameter_keys = "mu sigma".split()

    def __init__(  # pylint: disable = super-init-not-called, too-many-arguments
        self: MultivariateGaussian,
        mu: numpy.ndarray,
        sigma: numpy.ndarray,
        rng: numpy.random.Generator,
        Psi: typing.Union[float, numpy.ndarray, None] = .1,
        nu: typing.Union[float, numpy.ndarray, None] = 4.0,
        small=1.0e-100,
    ):
        # Check arguments
        self.n_states, self.dimension = mu.shape
        assert sigma.shape == (self.n_states, self.dimension, self.dimension)
        assert isinstance(rng, numpy.random.Generator)

        # Assign arguments to self
        self.mu = mu
        self.sigma = sigma
        self.inverse_sigma = numpy.empty(
            (self.n_states, self.dimension, self.dimension))
        self.norm = numpy.empty(self.n_states)
        for state in range(self.n_states):
            self.inverse_sigma[state, :, :] = numpy.linalg.inv(
                self.sigma[state, :, :])
            determinant = numpy.linalg.det(sigma[state, :, :])
            self.norm[state] = 1 / numpy.sqrt(
                (2 * numpy.pi)**self.dimension * determinant)
        self._rng = rng
        # pylint: disable=invalid-name
        if isinstance(Psi, numpy.ndarray) and Psi.shape == (self.n_states,
                                                            self.dimension,
                                                            self.dimension):
            self.Psi = Psi
        elif isinstance(Psi, numpy.ndarray) and Psi.shape == (self.dimension,
                                                              self.dimension):
            self.Psi = numpy.empty(
                (self.n_states, self.dimension, self.dimension))
            for state in range(self.n_states):
                self.Psi[state, :, :] = Psi
        else:
            assert isinstance(Psi, float)
            self.Psi = numpy.empty(
                (self.n_states, self.dimension, self.dimension))
            for state in range(self.n_states):
                self.Psi[state, :, :] = numpy.eye(self.dimension) * Psi
        if isinstance(nu, numpy.ndarray):
            assert nu.shape == (self.n_states,)
            self.nu = nu
        else:
            assert isinstance(nu, float)
            self.nu = numpy.ones(self.n_states) * nu
        self.small = small

    def random_out(self: MultivariateGaussian, state: int) -> numpy.ndarray:
        return self._rng.multivariate_normal(self.mu[state], self.sigma[state])

    def __str__(self: MultivariateGaussian) -> str:
        save = numpy.get_printoptions()['precision']
        numpy.set_printoptions(precision=3)
        result = [f'Model {self.__class__} instance\n']
        for state in range(self.n_states):
            result.append(f'For state {state}:\n')
            result.append(f' inverse_sigma = \n{self.inverse_sigma[state]}\n')
            result.append(f' mu = {self.mu[state]}')
            result.append(f' norm = {self.norm[state]:8.6f}\n')
        numpy.set_printoptions(precision=save)
        return ''.join(result)

    def _delta(self: MultivariateGaussian, t: int, state) -> numpy.ndarray:
        """Calculate the difference between mean and observed value.

        Args:
            t: Time
            s: State

        Returns:
            Difference between mean[s] and observation[t]

        This is separate from calculate so that subclass can use a
        variant.

        """
        return self._y[t] - self.mu[state]

    def calculate(self: MultivariateGaussian) -> numpy.ndarray:
        """
        Calculate and return likelihoods.

        Returns:
            self.p_y[t,s] = Prob(y[t]|state[t]=s)

        Assumes self.observe has assigned a single numpy.ndarray to self._y
        """

        # FixMe: Need term for prior probability of parameters
        length, _ = self._y.shape
        assert length == self.n_times, \
            'You must call observe before calling calculate.'
        for t in range(self.n_times):
            for state in range(self.n_states):
                delta = self._delta(t, state)
                d_q_d = float(
                    numpy.dot(delta, numpy.dot(self.inverse_sigma[state],
                                               delta)))
                if d_q_d > 300:  # Underflow
                    self._likelihood[t, state] = 0
                else:
                    self._likelihood[t, state] = self.norm[state] * numpy.exp(
                        -d_q_d / 2)
            if self._likelihood[t, :].sum() < self.small:
                raise ValueError(
                    f"""Observation is not plausible from any state.
self.likelihood[{t},:]={self._likelihood[t, :]}""")
        return self._likelihood

    def _new_mean(self: MultivariateGaussian, weights: numpy.ndarray,
                  w_sum: numpy.ndarray):
        """Calculate new means for all states.

        Args:
            weights: weights[t,s] = Prob(state[t]=s)
            w_sum: w_sum[s] = sum_t weight[t,s]

        """
        # ToDo: Eliminate .T
        self.mu = (numpy.inner(self._y.T, weights.T) / w_sum).T

    def reestimate(
        self: MultivariateGaussian,
        weights: numpy.ndarray,
    ):
        """
        Estimate new model parameters.  self._y already assigned

        Args:
            w: Weights; Prob(state[t]=s) given data and
                old model

        """
        wsum = weights.sum(axis=0)
        self._new_mean(weights, wsum)  # type: ignore
        for state in range(self.n_states):
            rrsum = numpy.zeros((self.dimension, self.dimension))
            for t in range(self.n_times):
                delta = self._delta(t, state)
                rrsum += weights[t, state] * numpy.outer(delta, delta)
            # ToDo: Is it + or - self.dimension?
            self.sigma[state, :, :] = (self.Psi[state] + rrsum) / (
                wsum[state] + self.nu[state] - self.dimension + 1)
            det = numpy.linalg.det(self.sigma[state])
            assert det > 0.0
            self.inverse_sigma[state, :, :] = numpy.linalg.inv(
                self.sigma[state])
            self.norm[state] = 1.0 / (numpy.sqrt(
                (2 * numpy.pi)**self.dimension * det))

    def log_prior(self: MultivariateGaussian) -> float:
        """Calculate and return the log of the prior probability.

        Discard constants that don't depend on self.sigma.  See
        https://en.wikipedia.org/wiki/Inverse-Wishart_distribution

        """
        result = 0.0
        for state in range(self.n_states):
            log_det = numpy.log(numpy.linalg.det(self.sigma[state]))
            trace = numpy.dot(self.Psi[state],
                              self.inverse_sigma[state]).trace()
            result += -(self.nu[state] + self.dimension +
                        1) * log_det / 2 - trace / 2
        return result

    def merge(self: MultivariateGaussian, raw_out):
        if isinstance(raw_out, numpy.ndarray):
            _, dimension = raw_out.shape
            assert dimension == len(self.dimension)
            return raw_out
        return numpy.array(raw_out).reshape(-1, self.dimension)


class LinearContext(GaussMAP):
    r"""Mean is a linear function of context.  Residuals are Gaussian.

    Args:
        coefficients[n_states, contex dimension]: Coefficients for context
        variance[n_states]: Residual variance for each state
        rng: Random number generator with state
        alpha: Denominator part of prior for variance
        beta: Numerator part of prior for variance
        small: Throw error if likelihood at any time is less than small

    Model: likelihood[t,s] = Normal(mu_{t,s}, var[s]) at _y[t]
           where mu_{t,s} = coefficients[s] \cdot context[t]


    You can't use this class by itself.  You must use a subclass, eg,
    AutoRegressive.

    """
    _parameter_keys = "coefficients variance".split()

    def __init__(  # pylint: disable = super-init-not-called
            self: LinearContext,
            coefficients: numpy.ndarray,
            variance: numpy.ndarray,
            rng: numpy.random.Generator,
            alpha: typing.Union[float, numpy.ndarray] = 4.0,
            beta: typing.Union[float, numpy.ndarray] = 16.0,
            small: float = 1.0e-100):

        self.n_states, self.context_dimension = coefficients.shape
        assert variance.shape == (self.n_states,)
        assert isinstance(rng, numpy.random.Generator)

        self.coefficients = coefficients
        self.variance = variance
        self.norm = numpy.empty(self.n_states)
        for state in range(self.n_states):
            self.norm[state] = 1 / numpy.sqrt(
                2 * numpy.pi * self.variance[state])
        self._rng = rng
        self._set_alpha_beta(alpha, beta)
        self.small = small

    def random_out(self: LinearContext, state: int) -> numpy.ndarray:
        raise NotImplementedError(
            f'random_out is not implemented by class {self.__class__.__name__}')

    def __str__(self: LinearContext) -> str:
        # FixMe: use append instead of +=
        save = numpy.get_printoptions()['precision']
        numpy.set_printoptions(precision=3)
        result = [f'Model {type(self)} instance\n']
        for state in range(self.n_states):
            result.append(f'For state {state}:\n')
            result.append(f' variance = \n{self.variance[state]}\n')
            result.append(f' coefficients = {self.coefficients}\n')
            result.append(f' norm = {self.norm[state]:8.6f}\n')
        numpy.set_printoptions(precision=save)
        return ''.join(result)

    def strip(self: LinearContext):
        """Free memory
        """
        self.context = None
        super().strip()

    def _concatenate(self: LinearContext, segments: typing.Sequence,
                     lengths) -> tuple:
        """Attach context to self and return the modified concatenated data
        and segment information.

        Args:
            segments: Independent measurement sequences.  Each sequence
            is a 1-d numpy array.
            lengths: Truncate each ith returned segment to lengths[i]

        Returns:
            (all_data, Segment boundaries)

        Subclasses must implement this method, and method must assign
        self.context.

        """

        raise NotImplementedError(
            f'_concatenate is not implemented by class {self.__class__.__name__}'
        )

    def calculate(self: LinearContext) -> numpy.ndarray:
        """
        Calculate and return likelihoods.

        Returns:
            likelihood where likelihood[t,s] = Prob(y[t]|state[t]=s)

        """
        # FixMe: Need term for log of prior probability
        assert self._y.shape == (self.n_times,)

        for t in range(self.n_times):
            delta = self._y[t] - numpy.dot(self.coefficients, self.context[t])
            exponent = -delta * delta / (2 * self.variance)
            if exponent.min() < -300:  # Underflow
                for state in range(self.n_states):
                    if exponent[state] < -300:
                        self._likelihood[t, state] = 0.0
                    else:
                        self._likelihood[t,
                                         state] = self.norm[state] * numpy.exp(
                                             exponent[state])
            else:
                self._likelihood[t, :] = self.norm * numpy.exp(exponent)

        self.debug_impossible_data()
        return self._likelihood

    # mypy objects: "incompatible with supertype Integerobservation"
    def reestimate(  # type: ignore
            self: LinearContext,
            weights: numpy.ndarray,
    ):
        """
        Estimate new model parameters.  Requires self._y already assigned

        Args:
            weights: weights[t,s] = Prob(state[t]=s) given data and
                old model

        """
        mask = weights >= self.small  # Small weights confuse the residual
        # calculation in least_squares()
        masked_weights = mask * weights
        wsum = masked_weights.sum(axis=0)
        root_weight = numpy.sqrt(
            masked_weights)  # n_times x n_states array of weights

        for state in range(self.n_states):
            w_y = root_weight[:, state] * self._y
            w_context = (root_weight[:, state] * self.context.T).T
            # pylint: disable = unused-variable
            fit, residuals, rank, singular_values = numpy.linalg.lstsq(
                w_context, w_y, rcond=None)
            if rank < self.context_dimension:
                # Keep old coefficients.  Use prior for variance
                print(
                    f"In LinearContext.reestimate {rank=}, {self.context_dimension=}, {state=}"
                )
                delta_squared = 0.0
                wsum[state] = 0.0
            else:
                self.coefficients[state, :] = fit
                delta = w_y - numpy.inner(w_context, fit)
                delta_squared = numpy.inner(delta, delta)
            numerator = 2 * self.beta[state] + delta_squared
            denominator = 2 * self.alpha[state] + 2 + wsum[state]
            self.variance[state] = numerator / denominator
            self.norm[state] = 1 / numpy.sqrt(
                2 * numpy.pi * self.variance[state])


class AutoRegressive(LinearContext):
    r"""Scalar autoregressive model with Gaussian residuals

    Args:
        ar_coefficients[n_states, ar_order]: Auto-regressive coefficients
        offset[n_states]: Affine offset for each state
        variance[n_states]: Residual variance for each state
        rng: Random number generator with state
        alpha: Denominator part of prior for variance
        beta: Numerator part of prior for variance
        small: Throw error if likelihood at any time is less than small

    For each state the model for the mean is affine with offset[s].

    Model: likelihood[t,s] = Normal(mu_{t,s}, var[s]) at _y[t]
           where mu_{t,s} = ar_coefficients[s] \cdot _y[t-n_ar:t] + offset[s]

    Notes: (1) ar_coefficients[s][0] multiplies y[t-n_ar].  (2) In
    method concatenate, each segment is shortened by ar_order so that
    each element of self._y has a context.  (3) If alpha and beta are
    floats, the same prior applies to all states.  If they are arrays,
    there is a different prior for each state.

    """
    _parameter_keys = "ar_coefficients offset variance".split()

    def __init__(  # pylint: disable = super-init-not-called
            self: AutoRegressive,
            ar_coefficients: numpy.ndarray,
            offset: numpy.ndarray,
            variance: numpy.ndarray,
            rng: numpy.random.Generator,
            alpha: typing.Union[float, numpy.ndarray] = 4.0,
            beta: typing.Union[float, numpy.ndarray] = 16.0,
            small: float = 1.0e-100):

        n_states, ar_order = ar_coefficients.shape
        assert offset.shape == (n_states,)
        context_dimension = ar_order + 1

        # Store ar_coefficients and offset in coefficients
        coefficients = numpy.empty((n_states, context_dimension))
        coefficients[:, :ar_order] = ar_coefficients
        coefficients[:, ar_order] = offset

        super().__init__(coefficients, variance, rng, alpha, beta, small)

    def random_out(self: AutoRegressive, state: int) -> numpy.ndarray:

        if not hasattr(self, 'history'):
            self.history = numpy.zeros(self.context_dimension)
            self.history[-1] = 1.0
        mu = numpy.dot(self.coefficients[state], self.history)
        result = self._rng.normal(mu, self.variance[state])
        self.history[:self.context_dimension -
                     2] = self.history[1:self.context_dimension - 1]
        self.history[self.context_dimension - 2] = result
        return result

    def __str__(self: AutoRegressive) -> str:
        save = numpy.get_printoptions()['precision']
        numpy.set_printoptions(precision=3)
        result = [f'Model {type(self)} instance\n']
        for state in range(self.n_states):
            result.append(f'For state {state}:\n')
            result.append(f' variance = \n{self.variance[state]}\n')
            result.append(f' coefficients = {self.coefficients[state, :]}')
            result.append(f' norm = {self.norm[state]:8.6f}\n')
        numpy.set_printoptions(precision=save)
        return ''.join(result)

    def _concatenate(self: AutoRegressive, segments: typing.Sequence,
                     lengths) -> tuple:
        """Attach context to self and return the modified concatenated data
        and segment information.

        Args:
            segments: Independent measurement sequences.  Each sequence
            is a 1-d numpy array.
            lengths: If not None, truncate each returned segment[i] to lengths[i]

        Returns:
            (all_data, Segment boundaries)

        Assigns self.context.

        This method shortens each segment by ar_order.  That enables
        having a true context for each element of self._y

        self.context will be used in calculate and reestimate.
        After values get assigned, context[t, :-1] = previous
        observations, and context[t, -1] = 1.0

        """
        ar_order = self.context_dimension - 1
        if lengths is None:
            lengths = numpy.array(list(
                len(segment) - ar_order for segment in segments),
                                  dtype=int)
        if lengths.min() < 1:
            raise ValueError(f'{lengths.min()=} is < 1. {lengths=}')
        t_seg = numpy.zeros(len(lengths) + 1, dtype=int)
        t_seg[1:] = lengths.cumsum()
        all_data = numpy.empty(t_seg[-1])
        self.context = numpy.ones((t_seg[-1], self.context_dimension))
        for t_start, t_end, segment in zip(t_seg[:-1], t_seg[1:], segments):
            all_data[t_start:t_end] = segment[ar_order:]
            for t in range(t_start, t_end):
                t_in = t - t_start
                self.context[t, :-1] = segment[t_in:t_in + ar_order]
                # The one in the last place of context gets multiplied
                # by the last elements of self.coefficients
                # in self.calculate().  It is an offset term.
        return all_data, t_seg


class VectorContext(MultivariateGaussian):
    r"""Multivariate Gaussian observations with mean = A * context vectors.

    Args:
        a_mean[n_states, out_dimension, context_dimension]: Map from context to mean
        covariance[n_states, out_dimension, out_dimension]: Residual covariance for
            each state
        rng: Random number generator with state
        nu: Inverse Wishart parameter
        Psi: Inverse Wishart parameter
        small: Raise error if total likelihood of any observation is
            less than small

    Model: likelihood[t,s] = Normal(mu_{t,s}, covariance[s]) at _y[t]
           where mu_{t,s} = a_mean[s] \cdot context[t]

    """

    def __init__(self: VectorContext, *args, **kwargs):
        self.a_mean = args[0]
        mu = self.a_mean[:, :, 0]  # Just a dummy with the right shape
        super().__init__(mu, *args[1:], **kwargs)
        del (self.mu)
        n_states, self.out_dimension, self.context_dimension = self.a_mean.shape
        assert n_states == self.n_states
        assert self.out_dimension == self.dimension
        self.total_dimension = self.context_dimension + self.out_dimension

    # self.random_out will have funny mean for each state

    def _delta(self: VectorContext, t: int, state: int) -> numpy.ndarray:
        r"""Calculate difference between observation and forecast.

        Args:
            t: Time
            state:

        Returns:
            observation[t] - A[s] \dot context[t]
        """
        mu_st = numpy.dot(self.a_mean[state], self._y[t, self.out_dimension:])
        return self._y[t, :self.out_dimension] - mu_st

    def _new_mean(self: VectorContext, weights: numpy.ndarray,
                  w_sum: numpy.ndarray):
        r"""Calculate new coefficients for all states

        Args:
            weights: weights[t,s] \forall t,s
            w_sum: w_sum[s] = \Sum_t w[t,s]

        Find A that minimizes sum_s sum_t
            w[s,t]*(observation[t] - A[s] * context[t])**2
        """
        n_t, dimension = self._y.shape
        assert dimension == self.total_dimension
        assert weights.shape == (n_t, self.n_states)
        root_w = numpy.sqrt(weights)
        for state in range(self.n_states):
            root_w_z = root_w[:, state] * self._y.T
            # Find a that minimizes |a@x-y| where y is the first part
            # of root_w_z and x is the last part.
            a_transpose, resids, rank, svals = numpy.linalg.lstsq(  # pylint: disable = unused-variable
                root_w_z[self.out_dimension:, :].T,
                root_w_z[:self.out_dimension, :].T,
                rcond=1e-10)
            assert a_transpose.shape == (self.context_dimension,
                                         self.out_dimension)
            self.a_mean[state] = a_transpose.T
        assert self.a_mean.shape == (self.n_states, self.out_dimension,
                                     self.context_dimension)


class VARG(VectorContext):
    r"""Vector AutoRegressive with Gaussian residuals.

    Special case of VectorContext in which the context is previous observations.

    For each state the model for the mean is affine with offset[s].

    Model:

        mu_{t,s} = ar_coefficients[s] \cdot _y[t-n_ar:t].reshape(-1) + offset[s]
        likelihood[t,s] = Normal(mu_{t,s}, var[s]) at _y[t]


    Notes: (1) ar_coefficients[s][0] multiplies y[t-n_ar,0].  (2) In
    method concatenate, each segment is shortened by ar_order so that
    each element of self._y has a context.
    """

    # self.observe is from base.BaseObservation which calls self._concatenate
    # self.calculate is from MultivariateGaussian which calls self._delta
    # self._delta is from VectorContext
    def __init__(self: VARG, *args, **kwargs):
        VectorContext.__init__(self, *args, **kwargs)
        assert (
            self.context_dimension
        ) % self.out_dimension == 1, f'{self.context_dimension=} {self.out_dimension=}'
        self.ar_order = int((self.context_dimension - 1) / self.out_dimension)

    def __str__(self: VARG) -> str:
        save = numpy.get_printoptions()['precision']
        numpy.set_printoptions(precision=3)
        result = [f'Model {self.__class__} instance\n']
        for state in range(self.n_states):
            result.append(f'For state {state}:\n')
            result.append(f' a_mean = {self.a_mean[state]}')
            result.append(f' inverse_sigma = \n{self.inverse_sigma[state]}\n')
            result.append(f' Psi = {self.Psi[state]}')
            result.append(f' nu = {self.nu[state]}')
            result.append(f' norm = {self.norm[state]:8.6f}\n')
        numpy.set_printoptions(precision=save)
        return ''.join(result)

    def random_out(self: VARG, state: int) -> numpy.ndarray:
        if not hasattr(self, 'history'):
            self.history = numpy.zeros(self.context_dimension)
            self.history[-1] = 1.0
        mu = numpy.dot(self.a_mean[state], self.history)
        result = self._rng.multivariate_normal(mu, self.sigma[state])
        length = self.out_dimension * (self.ar_order - 1)
        if length > 0:
            self.history[:length] = self.history[self.out_dimension:][:length]
        self.history[length:-1] = result
        return result

    def _concatenate(self: VARG, segments: typing.Sequence, lengths) -> tuple:
        """Attach context to self and return the modified concatenated data
        and segment information.

        Args:
            segments: Independent measurement sequences.  The shape of
                each sequence is (T, d) where T is the length and d ==
                self.out_dimension.
            lengths: If not None, truncate each returned segment[i] to lengths[i]

        Returns:
            (all_data, Segment boundaries)

        This method shortens each segment by ar_order.  That enables
        having a true context for each element of self._y

        The shape of all_data is (T_total, self.total_dimension).  The
        actual observations are all_data[:, :self.out_dimension), and
        the context is all_data[:, self.out_dimension:].

        """
        if lengths is None:
            lengths = numpy.array(list(
                len(segment) - self.ar_order for segment in segments),
                                  dtype=int)
        if lengths.min() < 1:
            raise ValueError(f'{lengths.min()=} is < 1. {lengths=}')
        t_seg = numpy.zeros(len(lengths) + 1, dtype=int)
        t_seg[1:] = lengths.cumsum()
        all_data = numpy.empty((t_seg[-1], self.total_dimension))

        # I duplicate data many times.  In cython, I could use
        # pointers instead of duplication.  self.context and
        # self.out_data are pointers into all_data
        self.context = all_data[:, self.out_dimension:]
        self.context[:, -1] = 1.0  # For offset term
        self.out_data = all_data[:, :self.out_dimension]

        for t_start, t_end, segment in zip(t_seg[:-1], t_seg[1:], segments):

            # Assign observations skipping first ar_order of segment
            end_in = self.ar_order + t_end - t_start
            self.out_data[t_start:t_end] = segment[self.ar_order:end_in]

            for t_out in range(t_start, t_end):
                t_in = t_out - t_start  # First t_in value will be 0

                # Assign context starting with first ar_order of segment
                self.context[t_out, :-1] = segment[t_in:t_in +
                                                   self.ar_order].reshape(-1)

        assert self.out_data.shape == (lengths.sum(), self.out_dimension)
        assert self.context.shape == (lengths.sum(),
                                      self.ar_order * self.out_dimension + 1)
        assert self.total_dimension == (self.ar_order +
                                        1) * self.out_dimension + 1
        assert all_data.shape == (lengths.sum(), self.total_dimension)
        return all_data, t_seg


# --------------------------------
# Local Variables:
# mode: python
# End:
