Hidden Markov Models
====================

HMM provides python code that implements the following algorithms
for hidden Markov models:

* **Forward**: Recursive estimation of state probabilities at each time t,
  given observation likelihoods for times 1 to t
* **Backward**: Combined with Forward, provides estimates of state
  probabilities at each time given *all* of the observation likelihoods
* **Train**: Implements Baum Welch algorithm which finds a local maximum
  of the likelihood of model parameters
* **Decode**: Implements the Viterbi algorithm for finding the most
  probable state sequence

Implementations of the above algorithms are independent of the
observation model.  HMM enables users to implement any observation
model by writing code for a class that provides methods for
calculating the likelihood of an observation given a state and for
reestimating model parameters given observations and state
likelihoods.  HMM includes implementations of the following
observation models:

* **IntegerObservation**: Integers in a finite range
* **Gauss**: Floats with state dependent mean and variance
* **GaussMAP**: Like Gauss but uses maximum a posteriori probability
  estimation
* **MultivariateGaussian**: Like GaussMAP but observations are vectors
  of floats
* **AutoRegressive**: Like GaussMAP but with linear autoregressive
  forecast and Gaussian residual
* **VARG**: Like AutoRegressive but for vector time series
* **JointObservation**: Supports combining observations of different types.

HMM also provides code for rudimentary Kalman filters and particle
filters.

I (Andy Fraser) started this project in 2021 to support a new version
of code for my book "Hidden Markov Models and Dynamical Systems".

Related links:

* **User Manual**: At `readthedocs <https://hmm-fraserphysics.readthedocs.io/en/latest/index.html>`_
* **API Documentation** At `readthedocs
  <https://hmm-fraserphysics.readthedocs.io/projects/API/en/latest/index.html>`_
* **License**: GPL-3.  You can redistribute and/or modify HMM under
  the terms of the GNU General Public License as published by the Free
  Software Foundation, either version 3 of the License, or (at your
  option) any later version.  See the file "License" in the root
  directory of the HMM distribution.
* **Book**: `Hidden Markov Models and Dynamical Systems
  <https://epubs.siam.org/doi/book/10.1137/1.9780898717747?mobileUi=0>`_,
  available from SIAM, motivates HMMs, explains the theory, and uses
  the code of this project for examples.
* **HMMDS**: Code (LaTeX, Python, Cython, etc) to build drafts of the
  book is available at `gitlab <https://gitlab.com/fraserphysics/hmmds>`_.
